/** @param {{ container: HTMLElement }} props */
declare function CollapseToolbar({ className, container, open, icon, maxWidth, tooltip, children }: {
    container: HTMLElement;
}): JSX.Element;
declare namespace CollapseToolbar {
    namespace propTypes {
        const children: PropTypes.Requireable<PropTypes.ReactNodeLike>;
        const className: PropTypes.Requireable<string>;
        const container: PropTypes.Requireable<any>;
        const icon: PropTypes.Validator<PropTypes.ReactElementLike>;
        const maxWidth: PropTypes.Requireable<number>;
        const open: PropTypes.Requireable<boolean>;
        const tooltip: PropTypes.Validator<string>;
    }
}
export default CollapseToolbar;
import PropTypes from "prop-types";
