export default function withHorizontalSlide(WrappedEl: any): React.ForwardRefExoticComponent<React.RefAttributes<any>>;
export const DEVICE_MODE: "pointer" | "touch";
import React from "react";
