"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DEVICE_MODE = void 0;
exports["default"] = withHorizontalSlide;

var _react = _interopRequireWildcard(require("react"));

var _clsx = _interopRequireDefault(require("clsx"));

var _TabScrollButton = _interopRequireDefault(require("@material-ui/core/TabScrollButton"));

var _styles = require("@material-ui/core/styles");

var _excluded = ["children", "className"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _extends() { _extends = Object.assign ? Object.assign.bind() : function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var DEVICE_MODE = matchMedia('(pointer:fine)').matches ? 'pointer' : 'touch';
exports.DEVICE_MODE = DEVICE_MODE;

function getScrollLeft(el, delta) {
  var _ref = el || {},
      scrollWidth = _ref.scrollWidth,
      clientWidth = _ref.clientWidth,
      scrollLeft = _ref.scrollLeft;

  return Math.max(0, Math.min(scrollLeft + delta, scrollWidth - clientWidth));
} //* Custom Hooks


var useStyles = (0, _styles.makeStyles)(function (theme) {
  return {
    root: {
      display: 'flex',
      justifyContent: 'flex-start',
      alignItems: 'center',
      overflow: 'hidden',
      whiteSpace: 'nowrap',
      '& > *[role=scroll-button]': {
        position: 'sticky',
        backdropFilter: "blur(".concat(theme.spacing(4), "px)"),
        borderRadius: '50%',
        height: theme.spacing(5),
        zIndex: theme.zIndex.appBar + 1,
        display: function display(_ref2) {
          var visible = _ref2.visible;
          return !visible ? 'none' : null;
        },
        '&:first-child': {
          margin: theme.spacing(0, 1, 0, 0),
          left: 0
        },
        '&:last-child': {
          margin: theme.spacing(0, 0, 0, 1),
          right: 0
        },
        '&.disabled': {
          background: theme.palette.action.disabledBackground,
          opacity: theme.palette.action.disabledOpacity
        }
      }
    }
  };
});

var useHorizontalSlide = function useHorizontalSlide(ref) {
  var slidedRef = (0, _react.useRef)();

  var _useState = (0, _react.useState)(0),
      _useState2 = _slicedToArray(_useState, 2),
      scrollLeft = _useState2[0],
      setScrollLeft = _useState2[1];

  var _useReducer = (0, _react.useReducer)(function (_visible, _ref3) {
    var sw = _ref3.scrollWidth,
        cw = _ref3.clientWidth;
    return sw > cw;
  }, false),
      _useReducer2 = _slicedToArray(_useReducer, 2),
      visible = _useReducer2[0],
      dispatch = _useReducer2[1];

  var _ref4 = slidedRef.current || {},
      scrollWidth = _ref4.scrollWidth,
      clientWidth = _ref4.clientWidth;

  var classes = useStyles({
    visible: visible
  });
  (0, _react.useImperativeHandle)(ref, function () {
    return slidedRef.current;
  }); //* Window Resize 時，判斷是否呈現 Scroll Button

  (0, _react.useEffect)(function () {
    var _global$window;

    var resize = function resize() {
      return dispatch(slidedRef.current);
    };

    (_global$window = global.window) === null || _global$window === void 0 ? void 0 : _global$window.addEventListener('resize', resize);
    resize();
    return function () {
      var _global$window2;

      return (_global$window2 = global.window) === null || _global$window2 === void 0 ? void 0 : _global$window2.removeEventListener('resize', resize);
    };
  }, []); //* State - scrollLeft 改變時，連帶變更 Element scrollLeft

  (0, _react.useEffect)(function () {
    if (slidedRef.current) {
      slidedRef.current.scrollLeft = scrollLeft;
    }
  }, [scrollLeft]);
  return [slidedRef, classes.root, {
    left: scrollLeft > 0,
    right: scrollLeft < scrollWidth - clientWidth
  }, {
    left: function left() {
      return setScrollLeft(getScrollLeft(slidedRef.current, -100));
    },
    right: function right() {
      return setScrollLeft(getScrollLeft(slidedRef.current, 100));
    },
    wheel: function wheel(e) {
      return setScrollLeft(getScrollLeft(slidedRef.current, e.deltaY));
    },
    touch: function touch(e) {
      var _e$touches = _slicedToArray(e.touches, 1),
          start = _e$touches[0].clientX;

      function handleSlide(event) {
        var _event$changedTouches = _slicedToArray(event.changedTouches, 1),
            end = _event$changedTouches[0].clientX;

        var delta = start - end;
        start = end;
        setScrollLeft(getScrollLeft(slidedRef.current, delta));
      }

      document.body.addEventListener('touchmove', handleSlide);
      document.body.addEventListener('touchend', function end() {
        document.body.removeEventListener('touchmove', handleSlide);
        document.body.removeEventListener('touchend', end);
      });
    }
  }];
}; //* Wrapper


function withHorizontalSlide(WrappedEl) {
  var Wrapper = /*#__PURE__*/_react["default"].forwardRef(function (_ref5, ref) {
    var children = _ref5.children,
        className = _ref5.className,
        props = _objectWithoutProperties(_ref5, _excluded);

    var _useHorizontalSlide = useHorizontalSlide(ref),
        _useHorizontalSlide2 = _slicedToArray(_useHorizontalSlide, 4),
        slidedRef = _useHorizontalSlide2[0],
        cls = _useHorizontalSlide2[1],
        enabled = _useHorizontalSlide2[2],
        handles = _useHorizontalSlide2[3];

    return /*#__PURE__*/_react["default"].createElement(WrappedEl, _extends({}, props, {
      ref: slidedRef,
      className: (0, _clsx["default"])(className, cls),
      onWheel: handles.wheel,
      onTouchStart: handles.touch
    }), DEVICE_MODE === 'pointer' && /*#__PURE__*/_react["default"].createElement(_TabScrollButton["default"], {
      role: "scroll-button",
      direction: "left",
      orientation: "horizontal",
      classes: {
        disabled: 'disabled'
      },
      disabled: !enabled.left,
      onClick: handles.left
    }), children, DEVICE_MODE === 'pointer' && /*#__PURE__*/_react["default"].createElement(_TabScrollButton["default"], {
      role: "scroll-button",
      direction: "right",
      orientation: "horizontal",
      classes: {
        disabled: 'disabled'
      },
      disabled: !enabled.right,
      onClick: handles.right
    }));
  });

  Wrapper.displayName = 'HorizontalSlideWrapper';
  Wrapper.Naked = WrappedEl;
  return Wrapper;
}