"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = CollapseToolbar;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _Collapse = _interopRequireDefault(require("@material-ui/core/Collapse"));

var _Fab = _interopRequireDefault(require("@material-ui/core/Fab"));

var _Toolbar = _interopRequireDefault(require("@material-ui/core/Toolbar"));

var _Tooltip = _interopRequireDefault(require("@material-ui/core/Tooltip"));

var _styles = require("@material-ui/core/styles");

var _clsx = _interopRequireDefault(require("clsx"));

var _shortid = require("shortid");

var _withHorizontalSlide = _interopRequireWildcard(require("./withHorizontalSlide"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _extends() { _extends = Object.assign ? Object.assign.bind() : function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

//* Custom Hooks
var useStyles = (0, _styles.makeStyles)(function (theme) {
  return {
    root: {
      display: 'flex',
      flexDirection: 'row',
      margin: theme.spacing(0, 0.5)
    },
    toolbar: {
      padding: theme.spacing(0, 0.5),
      borderRadius: "".concat(theme.spacing(3), "px / 50% !important"),
      minHeight: theme.spacing(6),
      maxWidth: function maxWidth(_ref) {
        var _maxWidth = _ref.maxWidth;
        return _maxWidth || '100%';
      },
      background: theme.palette.grey[800],
      '&:hover': {
        background: theme.palette.grey[700]
      },
      '& > button[role=icon]': {
        maxWidth: theme.spacing(5),
        minWidth: theme.spacing(5)
      },
      '& > *[role=content]': {
        maxWidth: '100%',
        padding: function padding(_ref2) {
          var collapsed = _ref2.collapsed;
          return theme.spacing(0, collapsed ? 0 : 2);
        },
        width: function width(_ref3) {
          var collapsed = _ref3.collapsed;
          return collapsed ? 0 : 'auto';
        },
        transition: theme.transitions.create(['width', 'padding']),
        '& > *[role=divider]': {
          margin: theme.spacing(0, 2)
        }
      }
    }
  };
}); //* Component

var HorizontalSlideToolbar = (0, _withHorizontalSlide["default"])(_Toolbar["default"]);
/** @param {{ container: HTMLElement }} props */

function CollapseToolbar(_ref4) {
  var className = _ref4.className,
      container = _ref4.container,
      _ref4$open = _ref4.open,
      open = _ref4$open === void 0 ? false : _ref4$open,
      icon = _ref4.icon,
      maxWidth = _ref4.maxWidth,
      tooltip = _ref4.tooltip,
      children = _ref4.children;

  var _useState = (0, _react.useState)(true),
      _useState2 = _slicedToArray(_useState, 2),
      collapsed = _useState2[0],
      setCollapsed = _useState2[1];

  var id = (0, _react.useMemo)(function () {
    return (0, _shortid.generate)();
  }, []);
  var classes = useStyles({
    collapsed: collapsed,
    maxWidth: maxWidth
  });
  (0, _react.useEffect)(function () {
    setCollapsed(!open ? true : collapsed);
  }, [open]);
  (0, _react.useEffect)(function () {
    if (!collapsed) {
      container === null || container === void 0 ? void 0 : container.dispatchEvent(new CustomEvent('toggle', {
        detail: {
          id: id
        }
      }));
    }
  }, [collapsed]);
  (0, _react.useEffect)(function () {
    if (container) {
      var toggleFn = function toggleFn(_ref5) {
        var targetId = _ref5.detail.id;
        return targetId !== id && setCollapsed(true);
      };

      container.addEventListener('toggle', toggleFn);
      return function () {
        return container.removeEventListener('toggle', toggleFn);
      };
    }
  }, [container]);
  return /*#__PURE__*/_react["default"].createElement(_Collapse["default"], {
    "in": open,
    classes: {
      wrapperInner: classes.root
    }
  }, /*#__PURE__*/_react["default"].createElement(_Toolbar["default"], _extends({
    disableGutters: true,
    variant: "dense",
    className: (0, _clsx["default"])(classes.toolbar, className)
  }, _withHorizontalSlide.DEVICE_MODE === 'pointer' && {
    onMouseLeave: function onMouseLeave() {
      return setCollapsed(true);
    }
  }), /*#__PURE__*/_react["default"].createElement(_Tooltip["default"], {
    title: tooltip
  }, /*#__PURE__*/_react["default"].createElement(_Fab["default"], _extends({
    role: "icon",
    size: "small",
    color: collapsed ? 'default' : 'primary'
  }, _withHorizontalSlide.DEVICE_MODE === 'touch' ? {
    onClick: function onClick() {
      return setCollapsed(!collapsed);
    }
  } : {
    onMouseEnter: function onMouseEnter() {
      return setCollapsed(false);
    }
  }), icon)), /*#__PURE__*/_react["default"].createElement(HorizontalSlideToolbar, {
    key: collapsed,
    role: "content",
    variant: "dense"
  }, children)));
}

CollapseToolbar.propTypes = {
  children: _propTypes["default"].node,
  className: _propTypes["default"].string,
  container: global.window ? _propTypes["default"].instanceOf(HTMLElement) : _propTypes["default"].any,
  icon: _propTypes["default"].element.isRequired,
  maxWidth: _propTypes["default"].number,
  open: _propTypes["default"].bool,
  tooltip: _propTypes["default"].string.isRequired
};