"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "Geography", {
  enumerable: true,
  get: function get() {
    return _Geography["default"];
  }
});

var _Geography = _interopRequireDefault(require("./Geography"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }