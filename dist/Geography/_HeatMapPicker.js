"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = HeatMapPicker;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _Button = _interopRequireDefault(require("@material-ui/core/Button"));

var _Divider = _interopRequireDefault(require("@material-ui/core/Divider"));

var _Gradient = _interopRequireDefault(require("@material-ui/icons/Gradient"));

var _Switch = _interopRequireDefault(require("@material-ui/core/Switch"));

var _Tooltip = _interopRequireDefault(require("@material-ui/core/Tooltip"));

var _styles = require("@material-ui/core/styles");

var _maplibreGl = require("maplibre-gl");

var _customs = require("./_customs");

var _SettingWrapper = require("./_SettingWrapper");

var _makeLocales = require("../_utils/makeLocales");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var HEAT_MAP_PROP_TYPES = {
  paint: _propTypes["default"].shape({
    'heatmap-weight': _customs.PROPERTY_VALUE_SPECIFICATION_PROPTYPES.isRequired,
    'heatmap-radius': _customs.PROPERTY_VALUE_SPECIFICATION_PROPTYPES.isRequired
  }).isRequired
}; //* Custom Hooks

var useStyles = (0, _styles.makeStyles)(function (theme) {
  return {
    button: {
      border: '0 !important',
      '& + button': {
        marginLeft: theme.spacing(1)
      }
    },
    checked: {
      transform: "translateX(".concat(theme.spacing(2), "px) !important")
    }
  };
}); //* Component

/** @param {{ map: MaplibreGl }} props */

function HeatMapPicker(_ref) {
  var options = _ref.options,
      _ref$value = _ref.value,
      value = _ref$value === void 0 ? null : _ref$value,
      _onChange = _ref.onChange;

  var _useLocales = (0, _makeLocales.useLocales)(),
      gt = _useLocales.getFixedT;

  var _useState = (0, _react.useState)(value || Object.keys(options)[0]),
      _useState2 = _slicedToArray(_useState, 2),
      selected = _useState2[0],
      setSelected = _useState2[1];

  var classes = useStyles();
  return /*#__PURE__*/_react["default"].createElement(_SettingWrapper.SettingToolbar, {
    tooltip: gt('btn-heatmap'),
    icon: /*#__PURE__*/_react["default"].createElement(_Gradient["default"], null)
  }, /*#__PURE__*/_react["default"].createElement(_Tooltip["default"], {
    title: gt("lbl-heatmap-".concat(value || 'off'), {
      mode: value
    })
  }, /*#__PURE__*/_react["default"].createElement(_Switch["default"], {
    classes: {
      checked: classes.checked
    },
    color: "primary",
    size: "small",
    checked: Boolean(value),
    onChange: function onChange(e) {
      return _onChange(e.target.checked ? selected : null);
    }
  })), value && Object.keys(options).length > 1 && /*#__PURE__*/_react["default"].createElement(_react["default"].Fragment, null, /*#__PURE__*/_react["default"].createElement(_Divider["default"], {
    role: "divider",
    flexItem: true,
    orientation: "vertical"
  }), Object.entries(options).map(function (_ref2) {
    var _ref3 = _slicedToArray(_ref2, 2),
        mode = _ref3[0],
        heatMapProps = _ref3[1];

    return /*#__PURE__*/_react["default"].createElement(_Button["default"], {
      key: mode,
      variant: "text",
      color: mode === value ? 'primary' : 'default',
      classes: {
        root: classes.button
      },
      onClick: function onClick() {
        setSelected(mode);

        _onChange(mode);
      }
    }, gt("lbl-heatmap-".concat(mode)));
  })));
}

HeatMapPicker.propTypes = {
  gradientColors: _propTypes["default"].arrayOf(_propTypes["default"].string.isRequired),
  value: _propTypes["default"].oneOf(['polygon', 'point']),
  onChange: _propTypes["default"].func,
  options: _propTypes["default"].exact({
    //* 相關設置可參考: https://docs.mapbox.com/mapbox-gl-js/example/heatmap-layer/
    point: _propTypes["default"].shape(HEAT_MAP_PROP_TYPES),
    area: _propTypes["default"].shape(_objectSpread(_objectSpread({}, HEAT_MAP_PROP_TYPES), {}, {
      source: _propTypes["default"].oneOf(['point', 'properties']) //* 當

    }))
  }).isRequired
};