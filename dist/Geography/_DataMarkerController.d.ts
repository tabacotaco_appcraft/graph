declare function DataMarkerController({ cluster, lables, data, value, onChange }: {
    cluster: any;
    lables: any;
    data?: any[];
    value: any;
    onChange: any;
}): JSX.Element;
declare namespace DataMarkerController {
    namespace propTypes {
        const lables: PropTypes.Requireable<{
            [x: string]: PropTypes.ReactNodeLike;
        }>;
        const onChange: PropTypes.Requireable<(...args: any[]) => any>;
        const cluster: PropTypes.Requireable<Required<PropTypes.InferProps<{
            base: PropTypes.Requireable<number>;
            multiple: PropTypes.Requireable<number>;
        }>>>;
        const value: PropTypes.Requireable<Required<PropTypes.InferProps<{
            id: PropTypes.Validator<string>;
            mode: PropTypes.Requireable<string>;
        }>>[]>;
        const data: PropTypes.Requireable<Required<PropTypes.InferProps<{
            id: PropTypes.Validator<string>;
            group: PropTypes.Requireable<string>;
            icon: PropTypes.Validator<string>;
            label: PropTypes.Requireable<string>;
            metadata: PropTypes.Requireable<object>;
            coordinates: PropTypes.Validator<Required<PropTypes.InferProps<{
                lat: PropTypes.Validator<number>;
                lng: PropTypes.Validator<number>;
                ts: PropTypes.Requireable<number | Date>;
            }>>[]>;
        }>>[]>;
    }
}
export default DataMarkerController;
import PropTypes from "prop-types";
