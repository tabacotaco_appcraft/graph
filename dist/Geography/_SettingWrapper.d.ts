export function SettingToolbar(props: any): JSX.Element;
declare function SettingWrapper({ children, map, open, gradient }: {
    children: any;
    map: any;
    open: any;
    gradient: any;
}): JSX.Element;
declare namespace SettingWrapper {
    namespace propTypes {
        const children: PropTypes.Validator<PropTypes.ReactNodeLike>;
        const gradient: PropTypes.Requireable<string[]>;
        const map: PropTypes.Requireable<MaplibreGl>;
        const open: PropTypes.Validator<boolean>;
    }
}
export default SettingWrapper;
export function useMap(): MaplibreGl;
export function useGradient(): any[];
export function useLayerHandles({ category, sourceIds, autofit }: {
    category: 'area' | 'marker';
    sourceIds: string[];
    autofit?: boolean;
}, initialFn: (category: string, sourceId: string, context: any) => Promise<Source>, eventsFn: (source: Source, context: any) => ({
    layer: string | [];
})[]): any[];
import PropTypes from "prop-types";
import { Map as MaplibreGl } from "maplibre-gl";
import { Source } from "maplibre-gl";
