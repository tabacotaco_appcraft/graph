"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = DataMarkerController;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _ArrowDropDown = _interopRequireDefault(require("@material-ui/icons/ArrowDropDown"));

var _Button = _interopRequireDefault(require("@material-ui/core/Button"));

var _ButtonGroup = _interopRequireDefault(require("@material-ui/core/ButtonGroup"));

var _Divider = _interopRequireDefault(require("@material-ui/core/Divider"));

var _MenuItem = _interopRequireDefault(require("@material-ui/core/MenuItem"));

var _MenuList = _interopRequireDefault(require("@material-ui/core/MenuList"));

var _Popover = _interopRequireDefault(require("@material-ui/core/Popover"));

var _RoomOutlined = _interopRequireDefault(require("@material-ui/icons/RoomOutlined"));

var _styles = require("@material-ui/core/styles");

var _set2 = _interopRequireDefault(require("lodash/set"));

var _shortid = require("shortid");

var _maplibreGl = require("maplibre-gl");

var _SettingWrapper = require("./_SettingWrapper");

var _makeLocales = require("../_utils/makeLocales");

var _excluded = ["type", "id"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var DEFAULT_GROUP = (0, _shortid.generate)();
var LAYOUT_MODES = ['standard', 'track', 'heatmap-point', 'heatmap-area']; //* Custom Hooks

var useGroupOptions = function () {
  function reducer(state, _ref) {
    var type = _ref.type,
        id = _ref.id,
        options = _objectWithoutProperties(_ref, _excluded);

    switch (type) {
      case 'databound':
        {
          var sourceIds = options.sourceIds;
          return sourceIds.map(function (sourceId) {
            var _ref2 = state.find(function (_ref3) {
              var groupId = _ref3.id;
              return groupId === sourceId;
            }) || {},
                _ref2$mode = _ref2.mode,
                mode = _ref2$mode === void 0 ? 'standard' : _ref2$mode,
                _ref2$enabled = _ref2.enabled,
                enabled = _ref2$enabled === void 0 ? true : _ref2$enabled;

            return {
              id: sourceId,
              mode: mode,
              enabled: enabled
            };
          });
        }

      case 'visible':
        return state.map(function (group) {
          return group.id === id ? (0, _set2["default"])(group, 'enabled', true) : group;
        });

      case 'hidden':
        return state.map(function (group) {
          return group.id === id ? (0, _set2["default"])(group, 'enabled', false) : group;
        });

      case 'mode':
        return state.map(function (group) {
          return group.id === id ? _objectSpread(_objectSpread({}, group), options) : group;
        });

      default:
    }

    return state;
  }

  return function (sourceIds, value, onChange) {
    var _useReducer = (0, _react.useReducer)(reducer, sourceIds.map(function (sourceId) {
      var _ref4 = (value || []).find(function (_ref5) {
        var id = _ref5.id;
        return id === sourceId;
      }) || {},
          _ref4$mode = _ref4.mode,
          mode = _ref4$mode === void 0 ? 'standard' : _ref4$mode,
          _ref4$enabled = _ref4.enabled,
          enabled = _ref4$enabled === void 0 ? true : _ref4$enabled;

      return {
        id: sourceId,
        mode: mode,
        enabled: enabled
      };
    })),
        _useReducer2 = _slicedToArray(_useReducer, 2),
        values = _useReducer2[0],
        dispatch = _useReducer2[1];

    (0, _react.useEffect)(function () {
      onChange === null || onChange === void 0 ? void 0 : onChange(values.reduce(function (result, _ref6) {
        var id = _ref6.id,
            mode = _ref6.mode,
            enabled = _ref6.enabled;
        return enabled ? result.concat({
          id: id,
          mode: mode
        }) : result;
      }, []));
    }, [values]);
    return [values, dispatch];
  };
}();

var useMarkerLayers = function useMarkerLayers(cluster, data) {
  var gradient = (0, _SettingWrapper.useGradient)();
  var theme = (0, _styles.useTheme)();
  var clusters = (0, _react.useMemo)(function () {
    var radius = 30;

    var _ref7 = cluster || {},
        _ref7$base = _ref7.base,
        base = _ref7$base === void 0 ? 100 : _ref7$base,
        _ref7$multiple = _ref7.multiple,
        multiple = _ref7$multiple === void 0 ? 5 : _ref7$multiple;

    return gradient.reduce(function (result, color, i) {
      if (i < gradient.length - 1) {
        var distance = base * Math.max(1, multiple * i);
        return {
          color: result.color.concat(color, distance),
          radius: result.radius.concat(radius + 10 * i, distance)
        };
      }

      return {
        color: result.color.concat(color),
        radius: result.radius.concat(radius + 10 * i)
      };
    }, {
      color: [],
      radius: []
    });
  }, []);

  var _useMemo = (0, _react.useMemo)(function () {
    var collection = data.reduce(function (result, _ref8) {
      var id = _ref8.id,
          coordinates = _ref8.coordinates,
          group = _ref8.group,
          icon = _ref8.icon,
          label = _ref8.label,
          metadata = _ref8.metadata;
      var sourceId = group || DEFAULT_GROUP;
      var features = result.get(sourceId) || [];
      features.push.apply(features, _toConsumableArray(coordinates.map(function (_ref9, seq) {
        var _objectSpread2;

        var lng = _ref9.lng,
            lat = _ref9.lat,
            ts = _ref9.ts;

        var _map = (Array.isArray(ts) ? ts : [ts]).map(function ($ts) {
          return new Date($ts || undefined).valueOf();
        }),
            _map2 = _slicedToArray(_map, 2),
            fm = _map2[0],
            _map2$ = _map2[1],
            to = _map2$ === void 0 ? fm : _map2$;

        return {
          type: 'Feature',
          geometry: {
            type: 'Point',
            coordinates: [lng, lat]
          },
          properties: _objectSpread(_objectSpread({}, metadata), {}, (_objectSpread2 = {}, _defineProperty(_objectSpread2, "".concat(sourceId, "_group"), sourceId), _defineProperty(_objectSpread2, "".concat(sourceId, "_icon"), icon), _defineProperty(_objectSpread2, "".concat(sourceId, "_id"), id), _defineProperty(_objectSpread2, "".concat(sourceId, "_label"), label), _defineProperty(_objectSpread2, "".concat(sourceId, "_seq"), seq), _defineProperty(_objectSpread2, "".concat(sourceId, "_ts"), {
            fm: fm,
            to: to
          }), _objectSpread2))
        };
      }).sort(function (_ref10, _ref11) {
        var ts1 = _ref10.properties["".concat(sourceId, "_ts")];

        var ts2 = _ref11.properties["".concat(sourceId, "_ts")];

        return ts1.fm - ts2.to;
      })));
      return result.set(sourceId, features);
    }, new Map());
    return [Array.from(collection.keys()), collection];
  }, [data]),
      _useMemo2 = _slicedToArray(_useMemo, 2),
      sourceIds = _useMemo2[0],
      sources = _useMemo2[1];

  return [sourceIds].concat(_toConsumableArray((0, _SettingWrapper.useLayerHandles)({
    category: 'marker',
    sourceIds: sourceIds,
    autofit: true
  },
  /** Create Source & Add Generate Layers
   *
   *  @param {'marker'} category
   *  @param {string} sourceId
   *  @param {{ map: MaplibreGl; }} context
   */
  function (category, sourceId, _ref12) {
    var LABEL_COLOR = _ref12.LABEL_COLOR,
        loadSource = _ref12.loadSource,
        addLayerGroup = _ref12.addLayerGroup;
    var haloColor = theme.palette.getContrastText(LABEL_COLOR);
    var source = loadSource(category, sourceId, {
      cluster: true,
      clusterMaxZoom: 14,
      data: {
        type: 'FeatureCollection',
        features: sources.get(sourceId)
      }
    });
    addLayerGroup(sourceId, [//* Cluster Circle Layer
    {
      type: 'circle',
      id: 'cluster',
      filter: ['has', 'point_count'],
      paint: {
        'circle-color': ['step', ['get', 'point_count']].concat(_toConsumableArray(clusters.color)),
        'circle-opacity': .8,
        'circle-pitch-alignment': 'map',
        'circle-radius': ['step', ['get', 'point_count']].concat(_toConsumableArray(clusters.radius)),
        'circle-stroke-color': haloColor,
        'circle-stroke-width': 2
      }
    }, //* Cluster Count Text Layer
    {
      type: 'symbol',
      id: 'count',
      filter: ['has', 'point_count'],
      paint: {
        'text-color': LABEL_COLOR,
        'text-halo-color': haloColor,
        'text-halo-width': 2,
        'text-opacity': .8
      },
      layout: {
        'symbol-z-order': 'viewport-y',
        'text-allow-overlap': true,
        'text-field': '{point_count_abbreviated}',
        'text-size': 16
      }
    }, //* Marker Layer
    {
      type: 'symbol',
      id: 'pin',
      filter: ['all', ['==', "".concat(sourceId, "_group"), sourceId], ['==', "".concat(sourceId, "_seq"), 0]],
      paint: {
        'text-color': LABEL_COLOR,
        'text-halo-color': haloColor,
        'text-halo-width': 2
      },
      layout: {
        'symbol-z-order': 'viewport-y',
        'icon-allow-overlap': true,
        'icon-anchor': 'bottom',
        'icon-image': ['get', "".concat(sourceId, "_icon")],
        'icon-size': 0.8,
        'text-allow-overlap': true,
        'text-anchor': 'top',
        'text-field': ['get', "".concat(sourceId, "_label")],
        'text-size': 14
      }
    }]);
    return Promise.resolve(source);
  }, function (source, _ref13) {
    var map = _ref13.map,
        resortLayers = _ref13.resortLayers;
    return [{
      layer: 'cluster',
      dblclick: function dblclick(layerId, e) {
        var _map$queryRenderedFea = map.queryRenderedFeatures(e.point, {
          layers: [layerId]
        }),
            _map$queryRenderedFea2 = _slicedToArray(_map$queryRenderedFea, 1),
            _map$queryRenderedFea3 = _map$queryRenderedFea2[0],
            geometry = _map$queryRenderedFea3.geometry,
            properties = _map$queryRenderedFea3.properties;

        source.getClusterExpansionZoom(properties.cluster_id, function (err, zoom) {
          if (!err) {
            map.easeTo({
              zoom: zoom,
              center: geometry.coordinates
            });
            resortLayers();
          }
        });
      }
    }, {
      layer: ['pin', 'cluster'],
      mouseenter: function mouseenter() {
        return map.getCanvas().style.cursor = 'pointer';
      },
      mouseleave: function mouseleave() {
        return map.getCanvas().style.cursor = '';
      }
    }];
  })));
};

var useStyles = (0, _styles.makeStyles)(function (theme) {
  return {
    group: {
      '& + *[role=group]': {
        marginLeft: theme.spacing(1)
      }
    },
    button: {
      border: '0 !important'
    }
  };
}); //* Components

var FitContentMenu = (0, _styles.withStyles)(function (theme) {
  return {
    root: {
      marginTop: theme.spacing(1),
      width: 'fit-content !important'
    }
  };
})(_MenuList["default"]);

function DataMarkerController(_ref14) {
  var cluster = _ref14.cluster,
      lables = _ref14.lables,
      _ref14$data = _ref14.data,
      data = _ref14$data === void 0 ? [] : _ref14$data,
      value = _ref14.value,
      onChange = _ref14.onChange;

  var _useLocales = (0, _makeLocales.useLocales)(),
      gt = _useLocales.getFixedT;

  var _useMarkerLayers = useMarkerLayers(cluster, data),
      _useMarkerLayers2 = _slicedToArray(_useMarkerLayers, 3),
      sourceIds = _useMarkerLayers2[0],
      addMarkerLayer = _useMarkerLayers2[1],
      removeMarkerLayer = _useMarkerLayers2[2];

  var _useGroupOptions = useGroupOptions(sourceIds, value, onChange),
      _useGroupOptions2 = _slicedToArray(_useGroupOptions, 2),
      values = _useGroupOptions2[0],
      dispatch = _useGroupOptions2[1];

  var _useState = (0, _react.useState)(null),
      _useState2 = _slicedToArray(_useState, 2),
      expanded = _useState2[0],
      setExpanded = _useState2[1];

  var classes = useStyles();
  return !data.length ? null : /*#__PURE__*/_react["default"].createElement(_SettingWrapper.SettingToolbar, {
    tooltip: gt('btn-marker'),
    icon: /*#__PURE__*/_react["default"].createElement(_RoomOutlined["default"], null)
  }, values.map(function (_ref15, i) {
    var id = _ref15.id,
        enabled = _ref15.enabled,
        mode = _ref15.mode;
    return /*#__PURE__*/_react["default"].createElement(_react["default"].Fragment, {
      key: id
    }, i > 0 && /*#__PURE__*/_react["default"].createElement(_Divider["default"], {
      flexItem: true,
      orientation: "vertical"
    }), /*#__PURE__*/_react["default"].createElement(_ButtonGroup["default"], {
      key: id,
      role: "group",
      variant: "text",
      className: classes.group,
      color: enabled ? 'primary' : 'default'
    }, /*#__PURE__*/_react["default"].createElement(_Button["default"], {
      classes: {
        root: classes.button
      },
      onClick: function onClick() {
        dispatch({
          type: enabled ? 'hidden' : 'visible',
          id: id
        });
        enabled ? removeMarkerLayer(id) : addMarkerLayer(id);
      }
    }, (lables === null || lables === void 0 ? void 0 : lables[id]) || id), /*#__PURE__*/_react["default"].createElement(_Button["default"], {
      onClick: function onClick(_ref16) {
        var currentTarget = _ref16.currentTarget;
        return setExpanded({
          anchorEl: currentTarget.closest('[role=group]'),
          id: id,
          mode: mode
        });
      }
    }, /*#__PURE__*/_react["default"].createElement(_ArrowDropDown["default"], null))));
  }), /*#__PURE__*/_react["default"].createElement(_Popover["default"], {
    anchorEl: expanded === null || expanded === void 0 ? void 0 : expanded.anchorEl,
    anchorOrigin: {
      vertical: 'bottom',
      horizontal: 'center'
    },
    transformOrigin: {
      vertical: 'top',
      horizontal: 'center'
    },
    open: Boolean(expanded),
    onClose: function onClose() {
      return setExpanded(null);
    },
    PaperProps: {
      component: FitContentMenu
    }
  }, LAYOUT_MODES.map(function (layout) {
    return /*#__PURE__*/_react["default"].createElement(_MenuItem["default"], {
      key: layout,
      selected: layout === (expanded === null || expanded === void 0 ? void 0 : expanded.mode),
      onClick: function onClick() {
        dispatch({
          type: 'mode',
          id: expanded.id,
          mode: layout
        });
        setExpanded(null);
      }
    }, gt("lbl-layout-".concat(layout)));
  })));
}

DataMarkerController.propTypes = {
  lables: _propTypes["default"].objectOf(_propTypes["default"].node.isRequired),
  onChange: _propTypes["default"].func,
  cluster: _propTypes["default"].exact({
    base: _propTypes["default"].number,
    multiple: _propTypes["default"].number
  }),
  value: _propTypes["default"].arrayOf(_propTypes["default"].exact({
    id: _propTypes["default"].string.isRequired,
    mode: _propTypes["default"].oneOf(LAYOUT_MODES)
  })),
  data: _propTypes["default"].arrayOf(_propTypes["default"].exact({
    id: _propTypes["default"].string.isRequired,
    group: _propTypes["default"].string,
    icon: _propTypes["default"].string.isRequired,
    label: _propTypes["default"].string,
    metadata: _propTypes["default"].object,
    coordinates: _propTypes["default"].arrayOf(_propTypes["default"].exact({
      lat: _propTypes["default"].number.isRequired,
      lng: _propTypes["default"].number.isRequired,
      ts: _propTypes["default"].oneOfType([_propTypes["default"].number, _propTypes["default"].instanceOf(Date)])
    }).isRequired).isRequired
  }))
};