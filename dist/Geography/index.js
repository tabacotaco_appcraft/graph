"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _Chip = _interopRequireDefault(require("@material-ui/core/Chip"));

var _FormControlLabel = _interopRequireDefault(require("@material-ui/core/FormControlLabel"));

var _Grid = _interopRequireDefault(require("@material-ui/core/Grid"));

var _IconButton = _interopRequireDefault(require("@material-ui/core/IconButton"));

var _List = _interopRequireDefault(require("@material-ui/core/List"));

var _ListItem = _interopRequireDefault(require("@material-ui/core/ListItem"));

var _ListItemText = _interopRequireDefault(require("@material-ui/core/ListItemText"));

var _Popper = _interopRequireDefault(require("@material-ui/core/Popper"));

var _SpeedDial = _interopRequireDefault(require("@material-ui/lab/SpeedDial"));

var _SpeedDialIcon = _interopRequireDefault(require("@material-ui/lab/SpeedDialIcon"));

var _Toolbar = _interopRequireDefault(require("@material-ui/core/Toolbar"));

var _Tooltip = _interopRequireDefault(require("@material-ui/core/Tooltip"));

var _Typography = _interopRequireDefault(require("@material-ui/core/Typography"));

var _styles = require("@material-ui/core/styles");

var _Add = _interopRequireDefault(require("@material-ui/icons/Add"));

var _Map = _interopRequireDefault(require("@material-ui/icons/Map"));

var _Navigation = _interopRequireDefault(require("@material-ui/icons/Navigation"));

var _Remove = _interopRequireDefault(require("@material-ui/icons/Remove"));

var _Tune = _interopRequireDefault(require("@material-ui/icons/Tune"));

var _omit2 = _interopRequireDefault(require("lodash/omit"));

var _clsx = _interopRequireDefault(require("clsx"));

var _maplibreGl = require("maplibre-gl");

var _DataMarkerController = _interopRequireDefault(require("./_DataMarkerController"));

var _HeatMapPicker = _interopRequireDefault(require("./_HeatMapPicker"));

var _PolygonSelection = _interopRequireDefault(require("./_PolygonSelection"));

var _SettingWrapper = _interopRequireDefault(require("./_SettingWrapper"));

var _StylePicker = _interopRequireWildcard(require("./_StylePicker"));

var _makeLocales = _interopRequireWildcard(require("../_utils/makeLocales"));

var _withHorizontalSlide = require("../_utils/withHorizontalSlide");

var _colors = require("../_utils/colors");

require("maplibre-gl/dist/maplibre-gl.css");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _extends() { _extends = Object.assign ? Object.assign.bind() : function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var LocalesEn = {
  "btn-area": "Areas",
  "btn-heatmap": "Heatmap",
  "btn-marker": "Markers",
  "btn-style": "Map Style",
  "btn-zoom-in": "Zoom In",
  "btn-zoom-out": "Zoom Out",
  "lbl-default-group": "Default",
  "lbl-heatmap-area": "Areas",
  "lbl-heatmap-off": "Closed",
  "lbl-heatmap-point": "Point",
  "lbl-high": "High",
  "lbl-layout-heatmap-area": "Area Heatmap",
  "lbl-layout-heatmap-point": "Point Heatmap",
  "lbl-layout-standard": "Standard",
  "lbl-layout-track": "Track",
  "lbl-low": "Low",
  "lbl-selected-count": "{{ count }} Displayed"
};
var LocalesZh = {
  "btn-area": "\u5340\u57DF",
  "btn-heatmap": "\u71B1\u5340\u6A21\u5F0F",
  "btn-marker": "\u5B9A\u4F4D\u6A19\u8A18",
  "btn-style": "\u5730\u5716\u6A23\u5F0F",
  "btn-zoom-in": "\u653E\u5927",
  "btn-zoom-out": "\u7E2E\u5C0F",
  "lbl-default-group": "\u9810\u8A2D",
  "lbl-heatmap-off": "\u95DC\u9589",
  "lbl-heatmap-point": "\u5F71\u97FF\u7BC4\u570D",
  "lbl-heatmap-area": "\u5340\u57DF\u6AA2\u8996",
  "lbl-high": "\u9AD8",
  "lbl-layout-heatmap-area": "\u5340\u57DF\u71B1\u5340\u6AA2\u8996",
  "lbl-layout-heatmap-point": "\u7BC4\u570D\u71B1\u5340\u6AA2\u8996",
  "lbl-layout-standard": "\u6A19\u6E96\u6A21\u5F0F",
  "lbl-layout-track": "\u8ECC\u8DE1\u6A21\u5F0F",
  "lbl-low": "\u4F4E",
  "lbl-selected-count": "\u5171\u986F\u793A {{ count }} \u500B\u5340\u57DF"
};
//* TS Namespace
var PROP_TYPES = {
  DataMarkerControllerProps: _propTypes["default"].shape(_DataMarkerController["default"].propTypes),
  HeatMapPickerProps: _propTypes["default"].shape(_HeatMapPicker["default"].propTypes),
  PolygonSelectionProps: _propTypes["default"].shape(_objectSpread({}, (0, _omit2["default"])(_PolygonSelection["default"].propTypes, ['heating']))),
  StylePickerProps: _propTypes["default"].shape(_StylePicker["default"].propTypes),
  className: _propTypes["default"].string,
  classes: _propTypes["default"].exact({
    root: _propTypes["default"].string,
    control: _propTypes["default"].string,
    setting: _propTypes["default"].string
  }),
  data: _propTypes["default"].arrayOf(_propTypes["default"].shape({
    coordinates: _propTypes["default"].arrayOf(_propTypes["default"].number).isRequired,
    properties: _propTypes["default"].object
  })),
  images: _propTypes["default"].objectOf(_propTypes["default"].string.isRequired),
  options: _propTypes["default"].shape({
    center: _propTypes["default"].arrayOf(_propTypes["default"].number).isRequired,
    zoom: _propTypes["default"].number.isRequired,
    style: _propTypes["default"].oneOfType([_propTypes["default"].string, _propTypes["default"].shape({
      layers: _propTypes["default"].arrayOf(_propTypes["default"].shape({
        id: _propTypes["default"].string.isRequired,
        source: _propTypes["default"].string.isRequired,
        type: _propTypes["default"].oneOf(['fill', 'line', 'symbol', 'circle', 'heatmap', 'fill-extrusion', 'raster', 'hillshade', 'background']).isRequired
      })),
      sources: _propTypes["default"].objectOf(_propTypes["default"].shape({
        description: _propTypes["default"].string.isRequired,
        type: _propTypes["default"].oneOf(['vector', 'raster', 'raster-dem', 'geojson', 'video', 'image']).isRequired,
        url: _propTypes["default"].arrayOf(_propTypes["default"].string),
        urls: _propTypes["default"].arrayOf(_propTypes["default"].string),
        coordinates: _propTypes["default"].arrayOf(_propTypes["default"].arrayOf(_propTypes["default"].number))
      }).isRequired).isRequired
    })])
  })
};
var AppcraftGeography;

(function (_AppcraftGeography) {
  var def;

  (function (_def) {})(def || (def = _AppcraftGeography.def || (_AppcraftGeography.def = {})));
})(AppcraftGeography || (AppcraftGeography = {})); //* Custom Hooks


var useMapboxGlCreate = function useMapboxGlCreate(ref, images, options, onSettingClose) {
  var _useState = (0, _react.useState)(null),
      _useState2 = _slicedToArray(_useState, 2),
      map = _useState2[0],
      setMap = _useState2[1];

  var _useState3 = (0, _react.useState)(options.bearing || 0),
      _useState4 = _slicedToArray(_useState3, 2),
      bearing = _useState4[0],
      setBearing = _useState4[1];

  var _useState5 = (0, _react.useState)(options.pitch || 0),
      _useState6 = _slicedToArray(_useState5, 2),
      pitch = _useState6[0],
      setPitch = _useState6[1];

  var containerRef = (0, _react.useRef)();
  var center = options.center,
      zoom = options.zoom,
      style = options.style;
  (0, _react.useImperativeHandle)(ref, function () {
    return map;
  });
  (0, _react.useMemo)(function () {
    return map === null || map === void 0 ? void 0 : map.setCenter(center);
  }, Object.values(center));
  (0, _react.useMemo)(function () {
    return map === null || map === void 0 ? void 0 : map.setZoom(Math.max(map === null || map === void 0 ? void 0 : map.getMinZoom(), Math.min(map === null || map === void 0 ? void 0 : map.getMaxZoom(), zoom)));
  }, [zoom]);
  (0, _react.useEffect)(function () {
    if (containerRef.current) {
      map === null || map === void 0 ? void 0 : map.remove();
      new _maplibreGl.Map(_objectSpread(_objectSpread({}, options), {}, {
        container: containerRef.current,
        crossSourceCollisions: false,
        style: typeof style === 'string' ? style : _objectSpread({
          version: 8
        }, style) // projection: 'globe'

      })).on('rotate', function (e) {
        return setBearing(e.target.getBearing());
      }).on('pitch', function (e) {
        return setPitch(e.target.getPitch());
      }).on('style.load', function (e) {
        return Object.entries(images || {}).forEach(function (_ref) {
          var _ref2 = _slicedToArray(_ref, 2),
              name = _ref2[0],
              url = _ref2[1];

          return (// @ts-ignore
            e.target.loadImage(url, function (err, img) {
              if (err) throw err;
              e.target.addImage(name, img);
            })
          );
        });
      }).on('load', function (e) {
        setMap(e.target);
        e.target.getCanvasContainer().addEventListener('click', onSettingClose);
      });
    }

    return function () {
      try {
        map === null || map === void 0 ? void 0 : map.remove();
      } catch (e) {}

      setMap(null);
    };
  }, []);
  return [containerRef, map, bearing, pitch];
}; // @ts-ignore


var useStyles = (0, _styles.makeStyles)(function (theme) {
  return {
    root: {
      position: 'relative',
      minHeight: '60vh',
      overflow: 'hidden',
      background: theme.palette.background.paper,
      borderRadius: theme.shape.borderRadius,
      '& details.mapboxgl-ctrl-attrib': {
        display: 'none !important'
      }
    },
    control: {
      position: 'absolute',
      bottom: theme.spacing(0),
      left: '50%',
      transform: 'translateX(-50%)',
      zIndex: theme.zIndex.appBar,
      background: 'none !important',
      '&::before': {
        content: '""',
        position: 'absolute',
        bottom: 0,
        left: '50%',
        top: '50%',
        transform: "translate(-50%, -".concat(theme.spacing(1.5), "px)"),
        backdropFilter: "blur(".concat(theme.spacing(2), "px)"),
        borderTopLeftRadius: '50%',
        borderTopRightRadius: '50%',
        width: 200,
        height: 200
      },
      '& button + *': {
        margin: theme.spacing(0, 0, 0, 0.5)
      },
      '& > button[role^=zoom]': {
        background: theme.palette.grey[800],
        borderRadius: '50%',
        color: theme.palette.getContrastText(theme.palette.grey[800]),
        fontSize: '1.5rem'
      },
      '& > *[role=compass] > button': {
        backdropFilter: "blur(".concat(theme.spacing(2), "px)"),
        borderRadius: '50%',
        color: theme.palette.error.dark,
        fontSize: '3rem',
        '& svg': {
          transform: function transform(_ref3) {
            var bearing = _ref3.bearing,
                pitch = _ref3.pitch;
            return "rotate3d(1, 0, 0, ".concat(pitch, "deg) rotate3d(0, 0, 1, ").concat(0 - bearing, "deg)");
          }
        },
        '& + span': {
          transform: "scale(0.7) translateY(".concat(theme.spacing(-1), "px)"),
          '& > *': {
            background: theme.palette.grey[800],
            color: theme.palette.getContrastText(theme.palette.grey[800])
          }
        }
      }
    },
    setting: {
      position: 'absolute',
      top: theme.spacing(2),
      left: theme.spacing(2),
      zIndex: theme.zIndex.appBar,
      '& > button': {
        background: theme.palette.grey[900],
        color: theme.palette.getContrastText(theme.palette.grey[900]),
        '&:hover': {
          background: theme.palette.grey[800]
        },
        '& + div[role=menu]': {
          width: theme.spacing(7),
          '& > * + *': {
            marginTop: theme.spacing(1)
          },
          '& button': {
            textTransform: 'capitalize'
          }
        }
      }
    },
    legend: {
      position: 'absolute !important',
      top: "".concat(theme.spacing(3.5), "px !important"),
      left: "".concat(theme.spacing(10), "px !important"),
      zIndex: theme.zIndex.tooltip,
      '& > *[role=legend]': {
        background: function background(_ref4) {
          var open = _ref4.open;
          return theme.palette.grey[open ? 800 : 900];
        },
        color: theme.palette.getContrastText(theme.palette.grey[900]),
        borderRadius: "".concat(theme.spacing(1.5), "px / 50% !important"),
        minHeight: theme.spacing(3.5),
        width: theme.spacing(20),
        padding: theme.spacing(0.25, 1.5),
        '& > *': {
          margin: theme.spacing(0, 0.5),
          '&[role=gradient]': {
            flexGrow: 1,
            width: function width(_ref5) {
              var _ref5$gradient = _ref5.gradient,
                  gradient = _ref5$gradient === void 0 ? [] : _ref5$gradient;
              return theme.spacing(2 * gradient.length);
            },
            background: function background(_ref6) {
              var _ref6$gradient = _ref6.gradient,
                  gradient = _ref6$gradient === void 0 ? [] : _ref6$gradient;
              return "linear-gradient(to right, ".concat(gradient.map(function (color) {
                return color;
              }), ")");
            },
            borderRadius: theme.spacing(0.25),
            height: theme.spacing(2.5),
            margin: theme.spacing(0, 0.5)
          }
        }
      }
    },
    legendtip: {
      padding: 0,
      '& > div[role=container]': {
        width: 'fit-content',
        '& > div[role=colors]': {
          borderLeft: "1px solid ".concat(theme.palette.divider),
          borderTopLeftRadius: theme.shape.borderRadius,
          borderBottomLeftRadius: theme.shape.borderRadius,
          background: function background(_ref7) {
            var _ref7$gradient = _ref7.gradient,
                gradient = _ref7$gradient === void 0 ? [] : _ref7$gradient;
            return "linear-gradient(to top, ".concat(gradient.map(function (color) {
              return color;
            }), ")");
          },
          width: theme.spacing(2.5)
        },
        '& > *[role=tip]': {
          display: 'flex',
          flexDirection: 'column-reverse'
        }
      }
    }
  };
}); //* Component

var Geography = /*#__PURE__*/_react["default"].forwardRef(function (_ref8, ref) {
  var DataMarkerControllerProps = _ref8.DataMarkerControllerProps,
      HeatMapPickerProps = _ref8.HeatMapPickerProps,
      PolygonSelectionProps = _ref8.PolygonSelectionProps,
      StylePickerProps = _ref8.StylePickerProps,
      className = _ref8.className,
      cls = _ref8.classes,
      images = _ref8.images,
      options = _ref8.options;

  var _useLocales = (0, _makeLocales.useLocales)(),
      gt = _useLocales.getFixedT;

  var _useState7 = (0, _react.useState)(false),
      _useState8 = _slicedToArray(_useState7, 2),
      open = _useState8[0],
      setOpen = _useState8[1];

  var _useState9 = (0, _react.useState)((HeatMapPickerProps === null || HeatMapPickerProps === void 0 ? void 0 : HeatMapPickerProps.value) || null),
      _useState10 = _slicedToArray(_useState9, 2),
      heatMapMode = _useState10[0],
      setHeatMapMode = _useState10[1];

  var _useMapboxGlCreate = useMapboxGlCreate(ref, images, _objectSpread(_objectSpread({}, options), {}, {
    style: (0, _StylePicker.getDefaultStyle)(options.style, StylePickerProps)
  }), function () {
    return setOpen(false);
  }),
      _useMapboxGlCreate2 = _slicedToArray(_useMapboxGlCreate, 4),
      containerRef = _useMapboxGlCreate2[0],
      map = _useMapboxGlCreate2[1],
      bearing = _useMapboxGlCreate2[2],
      pitch = _useMapboxGlCreate2[3];

  var heatMapOptions = (0, _react.useMemo)(function () {
    return Object.entries({
      polygon: Boolean(PolygonSelectionProps),
      point: Boolean(HeatMapPickerProps)
    }).reduce(function (result, _ref9) {
      var _ref10 = _slicedToArray(_ref9, 2),
          opt = _ref10[0],
          validation = _ref10[1];

      if (!validation) {
        delete result[opt];
      }

      return result;
    }, HeatMapPickerProps.options);
  }, [Boolean(PolygonSelectionProps), Boolean(HeatMapPickerProps)]);

  var _ref11 = (0, _colors.useGradientColors)(HeatMapPickerProps.gradientColors),
      _ref12 = _slicedToArray(_ref11, 2),
      gradient = _ref12[0],
      range = _ref12[1];

  var classes = useStyles({
    bearing: bearing,
    pitch: pitch,
    gradient: gradient,
    open: open
  });
  return /*#__PURE__*/_react["default"].createElement("div", {
    ref: containerRef,
    className: (0, _clsx["default"])(classes.root, className, cls === null || cls === void 0 ? void 0 : cls.root)
  },
  /*#__PURE__*/
  //* Control Bar
  _react["default"].createElement(_Toolbar["default"], {
    disableGutters: true,
    variant: "dense",
    className: (0, _clsx["default"])(classes.control, cls === null || cls === void 0 ? void 0 : cls.control),
    onClick: function onClick() {
      return setOpen(false);
    }
  }, /*#__PURE__*/_react["default"].createElement(_Tooltip["default"], {
    title: gt('btn-zoom-in')
  }, /*#__PURE__*/_react["default"].createElement(_IconButton["default"], {
    role: "zoom-in",
    size: "small",
    onClick: function onClick() {
      return map.zoomIn();
    }
  }, /*#__PURE__*/_react["default"].createElement(_Add["default"], {
    fontSize: "inherit"
  }))), /*#__PURE__*/_react["default"].createElement(_FormControlLabel["default"], {
    role: "compass",
    labelPlacement: "bottom",
    label: /*#__PURE__*/_react["default"].createElement(_Chip["default"], {
      size: "small",
      label: "".concat((90 - pitch).toFixed(2), "\xB0")
    }),
    control: /*#__PURE__*/_react["default"].createElement(_IconButton["default"], {
      size: "small",
      onClick: function onClick() {
        return map.resetNorthPitch();
      }
    }, /*#__PURE__*/_react["default"].createElement(_Navigation["default"], {
      fontSize: "inherit"
    }))
  }), /*#__PURE__*/_react["default"].createElement(_Tooltip["default"], {
    title: gt('btn-zoom-out')
  }, /*#__PURE__*/_react["default"].createElement(_IconButton["default"], {
    role: "zoom-out",
    size: "small",
    onClick: function onClick() {
      return map.zoomOut();
    }
  }, /*#__PURE__*/_react["default"].createElement(_Remove["default"], {
    fontSize: "inherit"
  }))), containerRef.current && /*#__PURE__*/_react["default"].createElement(_Popper["default"], {
    open: Boolean(heatMapMode),
    className: classes.legend,
    container: containerRef.current
  }, /*#__PURE__*/_react["default"].createElement(_Tooltip["default"], {
    classes: {
      tooltip: classes.legendtip
    },
    title: /*#__PURE__*/_react["default"].createElement(_Grid["default"], {
      container: true,
      role: "container"
    }, /*#__PURE__*/_react["default"].createElement(_Grid["default"], {
      item: true,
      role: "colors"
    }), /*#__PURE__*/_react["default"].createElement(_Grid["default"], {
      item: true,
      role: "tip",
      component: _List["default"],
      dense: true,
      disablePadding: true
    }, gradient.reduce(function (_ref13, color, i) {
      var node = _ref13.node,
          min = _ref13.num;
      var max = Math.ceil((min + range) * 100) / 100;
      node.push( /*#__PURE__*/_react["default"].createElement(_ListItem["default"], {
        key: color,
        divider: true,
        role: "item"
      }, /*#__PURE__*/_react["default"].createElement(_ListItemText["default"], {
        primaryTypographyProps: {
          variant: 'caption',
          color: 'inherit',
          align: 'center'
        },
        primary: "".concat(Math.max(0, min).toFixed(2), "% ~ ").concat((i === gradient.length - 1 ? 100 : max - 0.01).toFixed(2), "%")
      })));
      return {
        node: node,
        num: max
      };
    }, {
      node: [],
      num: 0
    }).node))
  }, /*#__PURE__*/_react["default"].createElement(_Toolbar["default"], {
    role: "legend"
  }, /*#__PURE__*/_react["default"].createElement(_Typography["default"], {
    variant: "subtitle2",
    color: "textSecondary",
    align: "center"
  }, gt('lbl-low')), /*#__PURE__*/_react["default"].createElement("span", {
    role: "gradient"
  }), /*#__PURE__*/_react["default"].createElement(_Typography["default"], {
    variant: "subtitle2",
    color: "textSecondary",
    align: "center"
  }, gt('lbl-high')))))), /*#__PURE__*/_react["default"].createElement(_SettingWrapper["default"], {
    map: map,
    open: open,
    gradient: gradient
  }, "//* Setting Board", /*#__PURE__*/_react["default"].createElement(_SpeedDial["default"], _extends({
    ariaLabel: "Map Settings",
    direction: "down",
    className: (0, _clsx["default"])(classes.setting, cls === null || cls === void 0 ? void 0 : cls.setting),
    open: open,
    icon: /*#__PURE__*/_react["default"].createElement(_SpeedDialIcon["default"], {
      icon: /*#__PURE__*/_react["default"].createElement(_Map["default"], null),
      openIcon: /*#__PURE__*/_react["default"].createElement(_Tune["default"], null)
    })
  }, _withHorizontalSlide.DEVICE_MODE === 'touch' ? {
    FabProps: {
      onClick: function onClick() {
        return setOpen(!open);
      }
    }
  } : {
    onMouseEnter: function onMouseEnter() {
      return setOpen(true);
    },
    onMouseLeave: function onMouseLeave() {
      return setOpen(false);
    }
  }), StylePickerProps && /*#__PURE__*/_react["default"].createElement(_StylePicker["default"], StylePickerProps), PolygonSelectionProps && /*#__PURE__*/_react["default"].createElement(_PolygonSelection["default"], _extends({}, PolygonSelectionProps, {
    heating: heatMapMode === 'polygon'
  })), DataMarkerControllerProps && /*#__PURE__*/_react["default"].createElement(_DataMarkerController["default"], DataMarkerControllerProps))));
});

Geography.displayName = 'Geography';
Geography.propTypes = PROP_TYPES;

var _default = (0, _makeLocales["default"])({
  en: LocalesEn,
  zh: LocalesZh
})(Geography);

exports["default"] = _default;