"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SettingToolbar = SettingToolbar;
exports["default"] = SettingWrapper;
exports.useMap = exports.useLayerHandles = exports.useGradient = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _styles = require("@material-ui/core/styles");

var _get2 = _interopRequireDefault(require("lodash/get"));

var _omit2 = _interopRequireDefault(require("lodash/omit"));

var _set2 = _interopRequireDefault(require("lodash/set"));

var _template2 = _interopRequireDefault(require("lodash/template"));

var _maplibreGl = require("maplibre-gl");

var _CollapseToolbar = _interopRequireDefault(require("../_utils/CollapseToolbar"));

var _excluded = ["id"],
    _excluded2 = ["map", "open"],
    _excluded3 = ["layer"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _extends() { _extends = Object.assign ? Object.assign.bind() : function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var Z_ORDER = ['area', 'marker']; //* Custom Hooks

var SettingContext = /*#__PURE__*/(0, _react.createContext)({
  map: null,
  open: false,
  gradient: [],
  loadSource: function loadSource() {
    return null;
  },
  getSources: function getSources() {
    return null;
  },
  getLayerIds: function getLayerIds() {
    return null;
  },
  addLayerGroup: function addLayerGroup() {
    return null;
  },
  removeLayerGroup: function removeLayerGroup() {
    return null;
  },
  resortLayers: function resortLayers() {
    return null;
  }
});
/** @param {MaplibreGl} map */

var useContextValue = function useContextValue(map, open, gradient) {
  return (0, _react.useMemo)(function () {
    function getSources(category) {
      var _map$getStyle = map.getStyle(),
          sources = _map$getStyle.sources;

      var categories = new Set(!category ? [] : Array.isArray(category) ? category : [category]);
      return Object.keys(sources).reduce(function (result, sourceId) {
        var source = map.getSource(sourceId);
        var code = (0, _get2["default"])(source, ['_data', 'properties', 'category']);
        return categories.size === 0 && code || categories.has(code) ? result.concat(source) : result;
      }, []);
    }

    return {
      map: map,
      open: open,
      gradient: gradient,
      getSources: getSources,
      loadSource: function loadSource(category, sourceId, _ref) {
        var specification = _extends({}, _ref);

        var source = map.getSource(sourceId) ? map.getSource(sourceId) : map.addSource(sourceId, _objectSpread({
          type: 'geojson'
        }, specification)).getSource(sourceId);
        source.setData((0, _set2["default"])(source._data, 'properties', {
          category: category,
          layerIds: []
        }));
        return source;
      },
      getLayerIds: function getLayerIds(sourceId) {
        var prefix = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
        var alloweds = (Array.isArray(prefix) ? prefix : [prefix]).map(function (str) {
          return new RegExp("^".concat(str, "__"));
        });
        var source = map.getSource(sourceId);
        var layerIds = (0, _get2["default"])(source, ['_data', 'properties', 'layerIds']);
        return (layerIds || []).reduce(function (result, id) {
          return alloweds.length === 0 || alloweds.some(function (regexp) {
            return regexp.test(id);
          }) ? result.concat(id) : result;
        }, []);
      },
      addLayerGroup: function addLayerGroup(sourceId, layers) {
        var source = map.getSource(sourceId);
        var features = source._data.features;
        source.setData((0, _set2["default"])(source._data, 'properties.layerIds', layers.map(function (_ref2) {
          var id = _ref2.id,
              layer = _objectWithoutProperties(_ref2, _excluded);

          var layerId = "".concat(id, "__").concat(sourceId);
          map.getLayer(layerId) && map.removeLayer(layerId);
          map.addLayer(_objectSpread(_objectSpread({}, layer), {}, {
            id: layerId,
            source: sourceId
          }));
          return layerId;
        })));
      },
      removeLayerGroup: function removeLayerGroup(sourceId) {
        var source = map.getSource(sourceId);
        var layerIds = source._data.properties.layerIds;
        layerIds.forEach(function (layerId) {
          return map.removeLayer(layerId);
        });
        source.setData((0, _set2["default"])(source._data, 'properties.layerIds', []));
      },
      resortLayers: function resortLayers() {
        return getSources().reduce(function (result, _ref3) {
          var _ref3$_data$propertie = _ref3._data.properties,
              category = _ref3$_data$propertie.category,
              layerIds = _ref3$_data$propertie.layerIds;
          return result.concat((layerIds || []).map(function (id) {
            return {
              id: id,
              category: category
            };
          }));
        }, []).sort(function (_ref4, _ref5) {
          var c1 = _ref4.category,
              id1 = _ref4.id;
          var c2 = _ref5.category,
              id2 = _ref5.id;
          var diff = Z_ORDER.indexOf(c1) - Z_ORDER.indexOf(c2);

          if (diff < 0) {
            map.moveLayer(id1, id2);
          } else if (diff > 0) {
            map.moveLayer(id2, id1);
          }

          return diff;
        });
      }
    };
  }, [map, open, gradient]);
};
/** @returns {MaplibreGl} */


var useMap = function useMap() {
  var _useContext = (0, _react.useContext)(SettingContext),
      map = _useContext.map;

  return map;
};

exports.useMap = useMap;

var useGradient = function useGradient() {
  var _useContext2 = (0, _react.useContext)(SettingContext),
      gradient = _useContext2.gradient;

  return gradient;
};

exports.useGradient = useGradient;

var useLayerHandles = function () {
  var getLabelColor = function getLabelColor(map) {
    return Object.values(map.style._layers).find(function (_ref6) {
      var id = _ref6.id,
          type = _ref6.type;
      return /^place_(city|label)/.test(id) && type === 'symbol';
    }).getPaintProperty('text-color');
  };
  /**
   *  @param {{ category: 'area' | 'marker'; sourceIds: string[]; autofit?: boolean; }} params
   *  @param {(category: string, sourceId: string, context: any) => Promise<Source>} initialFn
   *  @param {(source: Source, context: any) => ({ layer: string | []; })[]} eventsFn
   */


  return function (_ref7, initialFn, eventsFn) {
    var category = _ref7.category,
        sourceIds = _ref7.sourceIds,
        _ref7$autofit = _ref7.autofit,
        autofit = _ref7$autofit === void 0 ? false : _ref7$autofit;

    /** @type {{ map: MaplibreGl }} */
    var _useContext3 = (0, _react.useContext)(SettingContext),
        map = _useContext3.map,
        _open = _useContext3.open,
        context = _objectWithoutProperties(_useContext3, _excluded2);

    var getSources = context.getSources,
        getLayerIds = context.getLayerIds,
        removeLayerGroup = context.removeLayerGroup,
        resortLayers = context.resortLayers;
    var addLayers = (0, _react.useCallback)(initialFn, [].concat(_toConsumableArray(Object.values(context)), [sourceIds])); //* 當 Style 變動時, 需重新載入 GeoJSON

    (0, _react.useEffect)(function () {
      var render = function render() {
        return Promise.all(sourceIds.map(function (sourceId) {
          return addLayers(category, sourceId, _objectSpread(_objectSpread({}, context), {}, {
            map: map,
            LABEL_COLOR: getLabelColor(map)
          }));
        })).then(function (sources) {
          return sources.forEach(function (source) {
            return eventsFn === null || eventsFn === void 0 ? void 0 : eventsFn(source, _objectSpread(_objectSpread({}, context), {}, {
              category: category,
              map: map
            })).forEach(function (_ref8) {
              var layer = _ref8.layer,
                  handles = _objectWithoutProperties(_ref8, _excluded3);

              return (Array.isArray(layer) ? layer : [layer]).forEach(function (prefix) {
                return getLayerIds(source.id, prefix).forEach(function (layerId) {
                  return Object.entries(handles).forEach(function (_ref9) {
                    var _ref10 = _slicedToArray(_ref9, 2),
                        eventName = _ref10[0],
                        handleFn = _ref10[1];

                    var fn = function fn() {
                      for (var _len = arguments.length, e = new Array(_len), _key = 0; _key < _len; _key++) {
                        e[_key] = arguments[_key];
                      }

                      return handleFn.apply(void 0, [layerId].concat(e));
                    };

                    map.once("".concat(category, ".loaded"), function () {
                      return map.on(eventName, layerId, fn);
                    }).once("".concat(category, ".destroy"), function () {
                      return map.off(eventName, layerId, fn);
                    });
                  });
                });
              });
            });
          });
        }).then(function () {
          return resortLayers();
        }).then(function () {
          return (//* Auto Fit
            autofit && map.fitBounds(getSources(category).reduce(function (bounds, _ref11) {
              var features = _ref11._data.features;
              features.forEach(function (_ref12) {
                var _ref12$geometry = _ref12.geometry,
                    type = _ref12$geometry.type,
                    coordinates = _ref12$geometry.coordinates;

                switch (type) {
                  case 'Point':
                    {
                      bounds.extend(coordinates);
                      break;
                    }

                  default:
                    {
                      coordinates.flat().forEach(function (coordinate) {
                        return bounds.extend(coordinate);
                      });
                    }
                }
              });
              return bounds;
            }, new _maplibreGl.LngLatBounds()))
          );
        }).then(function () {
          return map.fire("".concat(category, ".loaded"));
        });
      }; //* 初始載入 GeoJSON


      render();
      map.on('style.load', render);
      return function () {
        map.off('style.load', render);
        map.fire("".concat(category, ".destroy"));
      };
    }, [sourceIds]);
    return [(0, _react.useCallback)(function (sourceId) {
      return addLayers(category, sourceId, _objectSpread(_objectSpread({}, context), {}, {
        map: map,
        LABEL_COLOR: getLabelColor(map)
      }));
    }, [context]), removeLayerGroup];
  };
}(); //* Components


exports.useLayerHandles = useLayerHandles;

function SettingToolbar(props) {
  var _useContext4 = (0, _react.useContext)(SettingContext),
      map = _useContext4.map,
      open = _useContext4.open;

  var theme = (0, _styles.useTheme)();
  return /*#__PURE__*/_react["default"].createElement(_CollapseToolbar["default"], _extends({}, props, {
    container: map.getContainer().querySelector('[aria-label="Map Settings"]'),
    open: open,
    maxWidth: map.getContainer().getBoundingClientRect().width - theme.spacing(4)
  }));
}

function SettingWrapper(_ref13) {
  var children = _ref13.children,
      map = _ref13.map,
      open = _ref13.open,
      gradient = _ref13.gradient;
  var value = useContextValue(map, open, gradient);
  return /*#__PURE__*/_react["default"].createElement(SettingContext.Provider, {
    value: value
  }, map && children);
}

SettingWrapper.propTypes = {
  children: _propTypes["default"].node.isRequired,
  gradient: _propTypes["default"].arrayOf(_propTypes["default"].string.isRequired),
  map: _propTypes["default"].instanceOf(_maplibreGl.Map),
  open: _propTypes["default"].bool.isRequired
};