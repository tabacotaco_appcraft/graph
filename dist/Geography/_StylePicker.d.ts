declare function StylePicker({ options, value: defaultValue, onChange }: {
    options: any;
    value: any;
    onChange: any;
}): JSX.Element;
declare namespace StylePicker {
    namespace propTypes {
        const value: PropTypes.Requireable<string>;
        const onChange: PropTypes.Requireable<(...args: any[]) => any>;
        const options: PropTypes.Validator<Required<PropTypes.InferProps<{
            id: PropTypes.Validator<string>;
            icon: PropTypes.Requireable<PropTypes.ReactElementLike>;
            description: PropTypes.Validator<string>;
            style: PropTypes.Validator<string | PropTypes.InferProps<{
                layers: PropTypes.Requireable<PropTypes.InferProps<{
                    id: PropTypes.Validator<string>;
                    source: PropTypes.Validator<string>;
                    type: PropTypes.Validator<string>;
                }>[]>;
                sources: PropTypes.Validator<{
                    [x: string]: PropTypes.InferProps<{
                        description: PropTypes.Validator<string>;
                        type: PropTypes.Validator<string>;
                        url: PropTypes.Requireable<string[]>;
                        urls: PropTypes.Requireable<string[]>;
                        coordinates: PropTypes.Requireable<number[][]>;
                    }>;
                }>;
            }>>;
        }>>[]>;
    }
}
export default StylePicker;
export function getDefaultStyle(defaultStyle: any, StylePickerProps: any): any;
import PropTypes from "prop-types";
