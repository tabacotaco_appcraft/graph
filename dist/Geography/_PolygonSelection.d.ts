declare function PolygonSelection({ heating, options, value: defaultValue, onChange }: {
    heating?: boolean;
    options: any;
    value: any;
    onChange: any;
}): JSX.Element;
declare namespace PolygonSelection {
    namespace propTypes {
        const heating: PropTypes.Requireable<boolean>;
        const onChange: PropTypes.Requireable<(...args: any[]) => any>;
        const value: PropTypes.Requireable<string | string[]>;
        const options: PropTypes.Requireable<Required<PropTypes.InferProps<{
            id: PropTypes.Validator<string>;
            icon: PropTypes.Requireable<PropTypes.ReactElementLike>;
            description: PropTypes.Validator<string>;
            label: PropTypes.Requireable<PropTypes.InferProps<{
                layout: PropTypes.Requireable<PropTypes.InferProps<{
                    'icon-image': PropTypes.Requireable<any[]>;
                    'text-field': PropTypes.Validator<any[]>;
                }>>;
                pain: PropTypes.Requireable<PropTypes.InferProps<{
                    'icon-opacity': PropTypes.Requireable<number>;
                    'icon-color': PropTypes.Requireable<string>;
                    'text-opacity': PropTypes.Requireable<number>;
                    'text-color': PropTypes.Requireable<string>;
                }>>;
            }>>;
            geojson: PropTypes.Validator<string | Required<PropTypes.InferProps<{
                type: PropTypes.Validator<string>;
                features: PropTypes.Validator<PropTypes.InferProps<{
                    type: PropTypes.Validator<string>;
                    properties: PropTypes.Requireable<PropTypes.InferProps<{
                        'fill-color': PropTypes.Requireable<string>;
                        'fill-opacity': PropTypes.Requireable<number>;
                        'fill-outline-color': PropTypes.Requireable<string>;
                    }>>;
                    geometry: PropTypes.Requireable<PropTypes.InferProps<{
                        type: PropTypes.Validator<string>;
                        coordinates: PropTypes.Validator<(number | number[])[][]>;
                    }>>;
                }>[]>;
            }>>>;
        }>>[]>;
    }
}
export default PolygonSelection;
import PropTypes from "prop-types";
