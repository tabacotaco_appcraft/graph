import React from 'react';
import PropTypes from 'prop-types';
import { Map as MaplibreGl } from 'maplibre-gl';
import 'maplibre-gl/dist/maplibre-gl.css';
declare const _default: React.ForwardRefExoticComponent<Pick<PropTypes.InferPropsInner<Pick<{
    DataMarkerControllerProps: PropTypes.Requireable<PropTypes.InferProps<{
        lables: PropTypes.Requireable<{
            [x: string]: PropTypes.ReactNodeLike;
        }>;
        onChange: PropTypes.Requireable<(...args: any[]) => any>;
        cluster: PropTypes.Requireable<Required<PropTypes.InferProps<{
            base: PropTypes.Requireable<number>;
            multiple: PropTypes.Requireable<number>;
        }>>>;
        value: PropTypes.Requireable<Required<PropTypes.InferProps<{
            id: PropTypes.Validator<string>;
            mode: PropTypes.Requireable<string>;
        }>>[]>;
        data: PropTypes.Requireable<Required<PropTypes.InferProps<{
            id: PropTypes.Validator<string>;
            group: PropTypes.Requireable<string>;
            icon: PropTypes.Validator<string>;
            label: PropTypes.Requireable<string>;
            metadata: PropTypes.Requireable<object>;
            coordinates: PropTypes.Validator<Required<PropTypes.InferProps<{
                lat: PropTypes.Validator<number>;
                lng: PropTypes.Validator<number>;
                ts: PropTypes.Requireable<number | Date>;
            }>>[]>;
        }>>[]>;
    }>>;
    HeatMapPickerProps: PropTypes.Requireable<PropTypes.InferProps<{
        gradientColors: PropTypes.Requireable<string[]>;
        value: PropTypes.Requireable<string>;
        onChange: PropTypes.Requireable<(...args: any[]) => any>;
        options: PropTypes.Validator<Required<PropTypes.InferProps<{
            point: PropTypes.Requireable<PropTypes.InferProps<{
                paint: PropTypes.Validator<PropTypes.InferProps<{
                    'heatmap-weight': PropTypes.Validator<any[]>;
                    'heatmap-radius': PropTypes.Validator<any[]>;
                }>>;
            }>>;
            area: PropTypes.Requireable<PropTypes.InferProps<{
                source: PropTypes.Requireable<string>;
                paint: PropTypes.Validator<PropTypes.InferProps<{
                    'heatmap-weight': PropTypes.Validator<any[]>;
                    'heatmap-radius': PropTypes.Validator<any[]>;
                }>>;
            }>>;
        }>>>;
    }>>;
    PolygonSelectionProps: PropTypes.Requireable<PropTypes.InferProps<{
        value: PropTypes.Requireable<string | string[]>;
        options: PropTypes.Requireable<Required<PropTypes.InferProps<{
            id: PropTypes.Validator<string>;
            icon: PropTypes.Requireable<PropTypes.ReactElementLike>;
            description: PropTypes.Validator<string>;
            label: PropTypes.Requireable<PropTypes.InferProps<{
                layout: PropTypes.Requireable<PropTypes.InferProps<{
                    'icon-image': PropTypes.Requireable<any[]>;
                    'text-field': PropTypes.Validator<any[]>;
                }>>;
                pain: PropTypes.Requireable<PropTypes.InferProps<{
                    'icon-opacity': PropTypes.Requireable<number>;
                    'icon-color': PropTypes.Requireable<string>;
                    'text-opacity': PropTypes.Requireable<number>;
                    'text-color': PropTypes.Requireable<string>;
                }>>;
            }>>;
            geojson: PropTypes.Validator<string | Required<PropTypes.InferProps<{
                type: PropTypes.Validator<string>;
                features: PropTypes.Validator<PropTypes.InferProps<{
                    type: PropTypes.Validator<string>;
                    properties: PropTypes.Requireable<PropTypes.InferProps<{
                        'fill-color': PropTypes.Requireable<string>;
                        'fill-opacity': PropTypes.Requireable<number>;
                        'fill-outline-color': PropTypes.Requireable<string>;
                    }>>;
                    geometry: PropTypes.Requireable<PropTypes.InferProps<{
                        type: PropTypes.Validator<string>;
                        coordinates: PropTypes.Validator<(number | number[])[][]>;
                    }>>;
                }>[]>;
            }>>>;
        }>>[]>;
        onChange: PropTypes.Requireable<(...args: any[]) => any>;
    }>>;
    StylePickerProps: PropTypes.Requireable<PropTypes.InferProps<{
        value: PropTypes.Requireable<string>;
        onChange: PropTypes.Requireable<(...args: any[]) => any>;
        options: PropTypes.Validator<Required<PropTypes.InferProps<{
            id: PropTypes.Validator<string>;
            icon: PropTypes.Requireable<PropTypes.ReactElementLike>;
            description: PropTypes.Validator<string>;
            style: PropTypes.Validator<string | PropTypes.InferProps<{
                layers: PropTypes.Requireable<PropTypes.InferProps<{
                    id: PropTypes.Validator<string>;
                    source: PropTypes.Validator<string>;
                    type: PropTypes.Validator<string>;
                }>[]>;
                sources: PropTypes.Validator<{
                    [x: string]: PropTypes.InferProps<{
                        description: PropTypes.Validator<string>;
                        type: PropTypes.Validator<string>;
                        url: PropTypes.Requireable<string[]>;
                        urls: PropTypes.Requireable<string[]>;
                        coordinates: PropTypes.Requireable<number[][]>;
                    }>;
                }>;
            }>>;
        }>>[]>;
    }>>;
    className: PropTypes.Requireable<string>;
    classes: PropTypes.Requireable<Required<PropTypes.InferProps<{
        root: PropTypes.Requireable<string>;
        control: PropTypes.Requireable<string>;
        setting: PropTypes.Requireable<string>;
    }>>>;
    data: PropTypes.Requireable<PropTypes.InferProps<{
        coordinates: PropTypes.Validator<number[]>;
        properties: PropTypes.Requireable<object>;
    }>[]>;
    images: PropTypes.Requireable<{
        [x: string]: string;
    }>;
    options: PropTypes.Requireable<PropTypes.InferProps<{
        center: PropTypes.Validator<[number, number]>;
        zoom: PropTypes.Validator<number>;
        style: PropTypes.Requireable<string | PropTypes.InferProps<{
            layers: PropTypes.Requireable<PropTypes.InferProps<{
                id: PropTypes.Validator<string>;
                source: PropTypes.Validator<string>;
                type: PropTypes.Validator<string>;
            }>[]>;
            sources: PropTypes.Validator<{
                [x: string]: PropTypes.InferProps<{
                    description: PropTypes.Validator<string>;
                    type: PropTypes.Validator<string>;
                    url: PropTypes.Requireable<string[]>;
                    urls: PropTypes.Requireable<string[]>;
                    coordinates: PropTypes.Requireable<number[][]>;
                }>;
            }>;
        }>>;
    }>>;
}, never>> & Partial<PropTypes.InferPropsInner<Pick<{
    DataMarkerControllerProps: PropTypes.Requireable<PropTypes.InferProps<{
        lables: PropTypes.Requireable<{
            [x: string]: PropTypes.ReactNodeLike;
        }>;
        onChange: PropTypes.Requireable<(...args: any[]) => any>;
        cluster: PropTypes.Requireable<Required<PropTypes.InferProps<{
            base: PropTypes.Requireable<number>;
            multiple: PropTypes.Requireable<number>;
        }>>>;
        value: PropTypes.Requireable<Required<PropTypes.InferProps<{
            id: PropTypes.Validator<string>;
            mode: PropTypes.Requireable<string>;
        }>>[]>;
        data: PropTypes.Requireable<Required<PropTypes.InferProps<{
            id: PropTypes.Validator<string>;
            group: PropTypes.Requireable<string>;
            icon: PropTypes.Validator<string>;
            label: PropTypes.Requireable<string>;
            metadata: PropTypes.Requireable<object>;
            coordinates: PropTypes.Validator<Required<PropTypes.InferProps<{
                lat: PropTypes.Validator<number>;
                lng: PropTypes.Validator<number>;
                ts: PropTypes.Requireable<number | Date>;
            }>>[]>;
        }>>[]>;
    }>>;
    HeatMapPickerProps: PropTypes.Requireable<PropTypes.InferProps<{
        gradientColors: PropTypes.Requireable<string[]>;
        value: PropTypes.Requireable<string>;
        onChange: PropTypes.Requireable<(...args: any[]) => any>;
        options: PropTypes.Validator<Required<PropTypes.InferProps<{
            point: PropTypes.Requireable<PropTypes.InferProps<{
                paint: PropTypes.Validator<PropTypes.InferProps<{
                    'heatmap-weight': PropTypes.Validator<any[]>;
                    'heatmap-radius': PropTypes.Validator<any[]>;
                }>>;
            }>>;
            area: PropTypes.Requireable<PropTypes.InferProps<{
                source: PropTypes.Requireable<string>;
                paint: PropTypes.Validator<PropTypes.InferProps<{
                    'heatmap-weight': PropTypes.Validator<any[]>;
                    'heatmap-radius': PropTypes.Validator<any[]>;
                }>>;
            }>>;
        }>>>;
    }>>;
    PolygonSelectionProps: PropTypes.Requireable<PropTypes.InferProps<{
        value: PropTypes.Requireable<string | string[]>;
        options: PropTypes.Requireable<Required<PropTypes.InferProps<{
            id: PropTypes.Validator<string>;
            icon: PropTypes.Requireable<PropTypes.ReactElementLike>;
            description: PropTypes.Validator<string>;
            label: PropTypes.Requireable<PropTypes.InferProps<{
                layout: PropTypes.Requireable<PropTypes.InferProps<{
                    'icon-image': PropTypes.Requireable<any[]>;
                    'text-field': PropTypes.Validator<any[]>;
                }>>;
                pain: PropTypes.Requireable<PropTypes.InferProps<{
                    'icon-opacity': PropTypes.Requireable<number>;
                    'icon-color': PropTypes.Requireable<string>;
                    'text-opacity': PropTypes.Requireable<number>;
                    'text-color': PropTypes.Requireable<string>;
                }>>;
            }>>;
            geojson: PropTypes.Validator<string | Required<PropTypes.InferProps<{
                type: PropTypes.Validator<string>;
                features: PropTypes.Validator<PropTypes.InferProps<{
                    type: PropTypes.Validator<string>;
                    properties: PropTypes.Requireable<PropTypes.InferProps<{
                        'fill-color': PropTypes.Requireable<string>;
                        'fill-opacity': PropTypes.Requireable<number>;
                        'fill-outline-color': PropTypes.Requireable<string>;
                    }>>;
                    geometry: PropTypes.Requireable<PropTypes.InferProps<{
                        type: PropTypes.Validator<string>;
                        coordinates: PropTypes.Validator<(number | number[])[][]>;
                    }>>;
                }>[]>;
            }>>>;
        }>>[]>;
        onChange: PropTypes.Requireable<(...args: any[]) => any>;
    }>>;
    StylePickerProps: PropTypes.Requireable<PropTypes.InferProps<{
        value: PropTypes.Requireable<string>;
        onChange: PropTypes.Requireable<(...args: any[]) => any>;
        options: PropTypes.Validator<Required<PropTypes.InferProps<{
            id: PropTypes.Validator<string>;
            icon: PropTypes.Requireable<PropTypes.ReactElementLike>;
            description: PropTypes.Validator<string>;
            style: PropTypes.Validator<string | PropTypes.InferProps<{
                layers: PropTypes.Requireable<PropTypes.InferProps<{
                    id: PropTypes.Validator<string>;
                    source: PropTypes.Validator<string>;
                    type: PropTypes.Validator<string>;
                }>[]>;
                sources: PropTypes.Validator<{
                    [x: string]: PropTypes.InferProps<{
                        description: PropTypes.Validator<string>;
                        type: PropTypes.Validator<string>;
                        url: PropTypes.Requireable<string[]>;
                        urls: PropTypes.Requireable<string[]>;
                        coordinates: PropTypes.Requireable<number[][]>;
                    }>;
                }>;
            }>>;
        }>>[]>;
    }>>;
    className: PropTypes.Requireable<string>;
    classes: PropTypes.Requireable<Required<PropTypes.InferProps<{
        root: PropTypes.Requireable<string>;
        control: PropTypes.Requireable<string>;
        setting: PropTypes.Requireable<string>;
    }>>>;
    data: PropTypes.Requireable<PropTypes.InferProps<{
        coordinates: PropTypes.Validator<number[]>;
        properties: PropTypes.Requireable<object>;
    }>[]>;
    images: PropTypes.Requireable<{
        [x: string]: string;
    }>;
    options: PropTypes.Requireable<PropTypes.InferProps<{
        center: PropTypes.Validator<[number, number]>;
        zoom: PropTypes.Validator<number>;
        style: PropTypes.Requireable<string | PropTypes.InferProps<{
            layers: PropTypes.Requireable<PropTypes.InferProps<{
                id: PropTypes.Validator<string>;
                source: PropTypes.Validator<string>;
                type: PropTypes.Validator<string>;
            }>[]>;
            sources: PropTypes.Validator<{
                [x: string]: PropTypes.InferProps<{
                    description: PropTypes.Validator<string>;
                    type: PropTypes.Validator<string>;
                    url: PropTypes.Requireable<string[]>;
                    urls: PropTypes.Requireable<string[]>;
                    coordinates: PropTypes.Requireable<number[][]>;
                }>;
            }>;
        }>>;
    }>>;
}, "data" | "className" | "options" | "classes" | "DataMarkerControllerProps" | "HeatMapPickerProps" | "PolygonSelectionProps" | "StylePickerProps" | "images">>> & React.RefAttributes<MaplibreGl> & {
    lang?: string;
    locales?: {
        [lang: string]: {
            [code: string]: string;
        };
    };
}, "data" | "key" | "className" | "options" | "classes" | keyof {
    lang?: string;
    locales?: {
        [lang: string]: {
            [code: string]: string;
        };
    };
} | "DataMarkerControllerProps" | "HeatMapPickerProps" | "PolygonSelectionProps" | "StylePickerProps" | "images"> & React.RefAttributes<any>>;
export default _default;
