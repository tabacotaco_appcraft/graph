/** @param {{ map: MaplibreGl }} props */
declare function HeatMapPicker({ options, value, onChange }: {
    map: MaplibreGl;
}): JSX.Element;
declare namespace HeatMapPicker {
    namespace propTypes {
        const gradientColors: PropTypes.Requireable<string[]>;
        const value: PropTypes.Requireable<string>;
        const onChange: PropTypes.Requireable<(...args: any[]) => any>;
        const options: PropTypes.Validator<Required<PropTypes.InferProps<{
            point: PropTypes.Requireable<PropTypes.InferProps<{
                paint: PropTypes.Validator<PropTypes.InferProps<{
                    'heatmap-weight': PropTypes.Validator<any[]>;
                    'heatmap-radius': PropTypes.Validator<any[]>;
                }>>;
            }>>;
            area: PropTypes.Requireable<PropTypes.InferProps<{
                source: PropTypes.Requireable<string>;
                paint: PropTypes.Validator<PropTypes.InferProps<{
                    'heatmap-weight': PropTypes.Validator<any[]>;
                    'heatmap-radius': PropTypes.Validator<any[]>;
                }>>;
            }>>;
        }>>>;
    }
}
export default HeatMapPicker;
import { Map as MaplibreGl } from "maplibre-gl";
import PropTypes from "prop-types";
