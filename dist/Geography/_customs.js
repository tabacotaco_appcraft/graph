"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.PROPERTY_VALUE_SPECIFICATION_PROPTYPES = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var PROPERTY_VALUE_SPECIFICATION_PROPTYPES = _propTypes["default"].arrayOf(_propTypes["default"].any.isRequired);

exports.PROPERTY_VALUE_SPECIFICATION_PROPTYPES = PROPERTY_VALUE_SPECIFICATION_PROPTYPES;