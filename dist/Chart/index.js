"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = exports.AppcraftChart = void 0;
Object.defineProperty(exports, "withSeriesData", {
  enumerable: true,
  get: function get() {
    return _withSeriesData["default"];
  }
});

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _styles = require("@material-ui/core/styles");

var _get2 = _interopRequireDefault(require("lodash/get"));

var _merge2 = _interopRequireDefault(require("lodash/merge"));

var _pick2 = _interopRequireDefault(require("lodash/pick"));

var _camelcase = _interopRequireDefault(require("camelcase"));

var _clsx = _interopRequireDefault(require("clsx"));

var _avocado = _interopRequireDefault(require("highcharts/themes/avocado"));

var _darkBlue = _interopRequireDefault(require("highcharts/themes/dark-blue"));

var _darkGreen = _interopRequireDefault(require("highcharts/themes/dark-green"));

var _darkUnica = _interopRequireDefault(require("highcharts/themes/dark-unica"));

var _gray = _interopRequireDefault(require("highcharts/themes/gray"));

var _grid = _interopRequireDefault(require("highcharts/themes/grid"));

var _highcharts = _interopRequireDefault(require("highcharts"));

var _sunset = _interopRequireDefault(require("highcharts/themes/sunset"));

var _map = _interopRequireDefault(require("highcharts/modules/map"));

var _gantt = _interopRequireDefault(require("highcharts/modules/gantt"));

var _stock = _interopRequireDefault(require("highcharts/modules/stock"));

var _withSeriesData = _interopRequireDefault(require("./withSeriesData"));

var _excluded = ["fontSize", "fontFamily", "fontWeight"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

//* TS Namespace
var AppcraftChart; //* Custom Hooks

exports.AppcraftChart = AppcraftChart;

(function (_AppcraftChart) {
  var def;

  (function (_def) {
    var proptypes = _def.proptypes = {
      className: _propTypes["default"].string,
      options: _propTypes["default"].shape({}).isRequired,
      theme: _propTypes["default"].oneOf(['avocado', 'dark-blue', 'dark-green', 'dark-unica', 'gray', 'grid', 'sunset']),
      type: _propTypes["default"].oneOf(['chart', 'ganttChart', 'mapChart', 'stockChart'])
    };
  })(def || (def = _AppcraftChart.def || (_AppcraftChart.def = {})));
})(AppcraftChart || (exports.AppcraftChart = AppcraftChart = {}));

(0, _gantt["default"])(_highcharts["default"]);
(0, _map["default"])(_highcharts["default"]);
(0, _stock["default"])(_highcharts["default"]);

var useHighchartsCreate = function () {
  var HIGHCHART_THEME = {
    AvocadoTheme: _avocado["default"],
    DarkBlueTheme: _darkBlue["default"],
    DarkGreenTheme: _darkGreen["default"],
    DarkUnicaTheme: _darkUnica["default"],
    GrayTheme: _gray["default"],
    GridTheme: _grid["default"],
    SunsetTheme: _sunset["default"]
  };
  return function (ref, type, options, themeCode) {
    var _useState = (0, _react.useState)(null),
        _useState2 = _slicedToArray(_useState, 2),
        highchart = _useState2[0],
        setHighchart = _useState2[1];

    var containerRef = (0, _react.useRef)();
    var theme = (0, _styles.useTheme)();
    (0, _react.useImperativeHandle)(ref, function () {
      return highchart;
    }, [highchart]);
    (0, _react.useEffect)(function () {
      var createFn = (0, _get2["default"])(_highcharts["default"], type);

      if (!containerRef.current) {
        setHighchart(null);
      } else {
        var withHighchartTheme = (0, _get2["default"])(HIGHCHART_THEME, "".concat((0, _camelcase["default"])(themeCode || (theme.palette.type === 'dark' ? 'dark-unica' : 'avocado'), {
          pascalCase: true
        }), "Theme"));
        withHighchartTheme({
          _modules: _objectSpread(_objectSpread({}, (0, _get2["default"])((0, _pick2["default"])(_highcharts["default"], [['_modules', 'Core/Globals.js'], ['_modules', 'Core/Utilities.js']]), '_modules')), {}, {
            'Core/DefaultOptions.js': {
              defaultOptions: {},
              setOptions: function setOptions(themeOptions) {
                return setHighchart(createFn(containerRef.current, (0, _merge2["default"])(themeOptions, {
                  chart: {
                    backgroundColor: theme.palette.background.paper
                  }
                }, options)));
              }
            }
          })
        });
      }

      return function () {
        try {
          highchart === null || highchart === void 0 ? void 0 : highchart.destroy();
        } catch (e) {}
      };
    }, [containerRef.current, type, themeCode]);
    return [containerRef, highchart];
  };
}();

var useStyles = (0, _styles.makeStyles)(function (theme) {
  var makeFontImportant = function makeFontImportant(_ref) {
    var fontSize = _ref.fontSize,
        fontFamily = _ref.fontFamily,
        fontWeight = _ref.fontWeight,
        styles = _objectWithoutProperties(_ref, _excluded);

    return _objectSpread(_objectSpread(_objectSpread(_objectSpread(_objectSpread({}, styles), fontSize && {
      fontSize: "".concat(fontSize, " !important")
    }), fontFamily && {
      fontFamily: "".concat(fontFamily, " !important")
    }), fontWeight && {
      fontWeight: "".concat(fontWeight, " !important")
    }), {}, {
      textTransform: 'capitalize !important',
      fill: "".concat(theme.palette.getContrastText(theme.palette.background.paper), " !important")
    });
  };

  return {
    root: {
      background: theme.palette.background.paper,
      borderRadius: theme.shape.borderRadius,
      width: '100%',
      '& text.highcharts-title': makeFontImportant(theme.typography.h6),
      '& text.highcharts-subtitle': makeFontImportant(theme.typography.subtitle2),
      '& text.highcharts-axis-title': makeFontImportant(theme.typography.subtitle2),
      '& text.highcharts-plot-line-label': makeFontImportant(theme.typography.caption),
      '& g.highcharts-legend-item > text': makeFontImportant(theme.typography.subtitle2),
      '& g.highcharts-axis-labels > text': makeFontImportant(theme.typography.body2),
      '& path.highcharts-tick, & path.highcharts-axis-line, & path.highcharts-grid-line': {
        stroke: "".concat(theme.palette.divider, " !important")
      },
      '& path.highcharts-label-icon': {
        fill: "".concat(theme.palette.primary.main, " !important")
      },
      '& rect.highcharts-background': {
        strokeWidth: '0 !important'
      },
      '& rect.highcharts-plot-background, & rect.highcharts-legend-box': {
        fill: "".concat(theme.palette.background.paper, " !important")
      },
      '& text.highcharts-credits': {
        display: 'none !important'
      }
    }
  };
}); //* Component

var Chart = /*#__PURE__*/_react["default"].forwardRef(function (_ref2, ref) {
  var className = _ref2.className,
      options = _ref2.options,
      theme = _ref2.theme,
      _ref2$type = _ref2.type,
      type = _ref2$type === void 0 ? 'chart' : _ref2$type;

  var _useHighchartsCreate = useHighchartsCreate(ref, type, options, theme),
      _useHighchartsCreate2 = _slicedToArray(_useHighchartsCreate, 1),
      containerRef = _useHighchartsCreate2[0];

  var classes = useStyles();
  return /*#__PURE__*/_react["default"].createElement("div", {
    ref: containerRef,
    className: (0, _clsx["default"])(classes.root, className)
  });
});

Chart.propTypes = AppcraftChart.def.proptypes;
var _default = Chart;
exports["default"] = _default;