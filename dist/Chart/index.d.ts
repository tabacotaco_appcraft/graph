import React from 'react';
import PropTypes from 'prop-types';
import Highcharts from 'highcharts';
import withSeriesData from './withSeriesData';
export declare namespace AppcraftChart {
    namespace def {
        const proptypes: {
            className: PropTypes.Requireable<string>;
            options: PropTypes.Validator<PropTypes.InferProps<{}>>;
            theme: PropTypes.Requireable<string>;
            type: PropTypes.Requireable<string>;
        };
        type Props = PropTypes.InferProps<typeof proptypes>;
        interface PropsWithData extends Omit<Props, 'type'> {
            data?: Record<string, any>[];
        }
    }
}
declare const Chart: React.ForwardRefExoticComponent<PropTypes.InferPropsInner<Pick<{
    className: PropTypes.Requireable<string>;
    options: PropTypes.Validator<PropTypes.InferProps<{}>>;
    theme: PropTypes.Requireable<string>;
    type: PropTypes.Requireable<string>;
}, never>> & Partial<PropTypes.InferPropsInner<Pick<{
    className: PropTypes.Requireable<string>;
    options: PropTypes.Validator<PropTypes.InferProps<{}>>;
    theme: PropTypes.Requireable<string>;
    type: PropTypes.Requireable<string>;
}, "type" | "className" | "options" | "theme">>> & React.RefAttributes<Highcharts.Chart>>;
export default Chart;
export { withSeriesData };
