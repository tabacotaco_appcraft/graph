import React from 'react';
import PropTypes from 'prop-types';
import Chart, { AppcraftChart } from './index';
declare const CONFIG_PROP_TYPES: {
    series: PropTypes.Requireable<string>;
    stack: PropTypes.Requireable<string>;
    x: PropTypes.Requireable<string>;
    aggregate: PropTypes.Requireable<"max" | "min" | "sum" | "count" | "avg">;
    y: PropTypes.Requireable<string[]>;
};
declare const withSeriesData: {
    ({ aggregate, series, stack, x, y }: PropTypes.InferProps<typeof CONFIG_PROP_TYPES>): (ChartElement: typeof Chart) => React.FC<AppcraftChart.def.PropsWithData>;
    configTypes: {
        series: PropTypes.Requireable<string>;
        stack: PropTypes.Requireable<string>;
        x: PropTypes.Requireable<string>;
        aggregate: PropTypes.Requireable<"max" | "min" | "sum" | "count" | "avg">;
        y: PropTypes.Requireable<string[]>;
    };
};
export default withSeriesData;
