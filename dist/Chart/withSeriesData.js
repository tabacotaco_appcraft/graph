"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _get2 = _interopRequireDefault(require("lodash/get"));

var _merge2 = _interopRequireDefault(require("lodash/merge"));

var _set2 = _interopRequireDefault(require("lodash/set"));

var _template2 = _interopRequireDefault(require("lodash/template"));

var _shortid = require("shortid");

var _excluded = ["data", "options"],
    _excluded2 = ["data"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _extends() { _extends = Object.assign ? Object.assign.bind() : function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//* Definitions
var CONFIG_PROP_TYPES = {
  series: _propTypes["default"].string,
  stack: _propTypes["default"].string,
  x: _propTypes["default"].string,
  aggregate: _propTypes["default"].oneOf(['avg', 'count', 'max', 'min', 'sum']),
  y: _propTypes["default"].arrayOf(_propTypes["default"].string)
}; //* HOC

var withSeriesData = function withSeriesData(_ref) {
  var _ref$aggregate = _ref.aggregate,
      aggregate = _ref$aggregate === void 0 ? 'sum' : _ref$aggregate,
      series = _ref.series,
      stack = _ref.stack,
      x = _ref.x,
      y = _ref.y;

  function compiled(template, params) {
    var interpolate = /{{([\s\S]+?)}}/g;

    try {
      return (0, _template2["default"])(interpolate.test(template) ? template : "{{".concat(template, "}}"), {
        interpolate: interpolate
      })(params) || null;
    } catch (e) {
      return null;
    }
  }

  function generator(defaultCategories, data) {
    var categories = new Set(defaultCategories.map(function (c) {
      return c.toString();
    }));
    var isCategoriesUndifined = categories.size === 0;
    return [Array.from(data //* Split Data
    .reduce(function (__, rowData) {
      return y.reduce(function (result, field) {
        var categoryName = compiled(x, rowData);

        if (categoryName && (isCategoriesUndifined || categories.has(categoryName))) {
          var value = (0, _get2["default"])(rowData, field) || 0;
          var refX = {
            field: x,
            value: categoryName
          };
          var refY = {
            field: field,
            value: value
          };
          var stackName = compiled(stack, _objectSpread(_objectSpread({}, rowData), {}, {
            series: {
              x: refX,
              y: refY
            }
          }));

          if (isCategoriesUndifined) {
            categories.add(categoryName);
          }

          result.push({
            name: categoryName,
            properties: rowData,
            y: value,
            series: {
              name: compiled(series, _objectSpread(_objectSpread({}, rowData), {}, {
                series: {
                  stack: stackName,
                  x: refX,
                  y: refY
                }
              })) || '',
              stack: stackName,
              x: refX,
              y: refY
            }
          });
        }

        return result;
      }, __);
    }, []) //* Categorize
    .reduce(function (result, _ref2) {
      var _ref2$series = _ref2.series,
          seriesName = _ref2$series.name,
          stackName = _ref2$series.stack,
          properties = _ref2.properties,
          categoryName = _ref2.name,
          y = _ref2.y;
      var key = JSON.stringify({
        name: seriesName,
        stack: stackName
      });

      var _ref3 = result.get(key) || {},
          _ref3$data = _ref3.data,
          seriesData = _ref3$data === void 0 ? new Map() : _ref3$data;

      var categoryData = seriesData.get(categoryName) || {
        name: categoryName,
        y: 0
      };

      switch (aggregate) {
        case 'avg':
          {
            (0, _set2["default"])(categoryData, 'y', (categoryData.y + y) / 2);
            break;
          }

        case 'count':
          {
            (0, _set2["default"])(categoryData, 'y', categoryData.y + 1);
            break;
          }

        case 'max':
          {
            (0, _set2["default"])(categoryData, 'y', Math.max(categoryData.y, y));
            break;
          }

        case 'min':
          {
            (0, _set2["default"])(categoryData, 'y', Math.min(categoryData.y, y));
            break;
          }

        default:
          {
            (0, _set2["default"])(categoryData, 'y', categoryData.y + y);
          }
      }

      return result.set(key, _objectSpread(_objectSpread({
        data: seriesData.set(categoryName, categoryData)
      }, seriesName && {
        name: seriesName
      }), stackName && {
        stack: stackName
      }));
    }, new Map()).values()), isCategoriesUndifined ? Array.from(categories).sort() : Array.from(categories)];
  }

  return function (ChartElement) {
    var SeriesData = /*#__PURE__*/_react["default"].forwardRef(function (_ref4, ref) {
      var data = _ref4.data,
          options = _ref4.options,
          props = _objectWithoutProperties(_ref4, _excluded);

      var defaultCategories = (0, _get2["default"])(options, ['xAxis', 'categories']);

      var _useMemo = (0, _react.useMemo)(function () {
        var _generator = generator(defaultCategories || [], data || []),
            _generator2 = _slicedToArray(_generator, 2),
            preSeries = _generator2[0],
            categories = _generator2[1];

        return {
          queue: (0, _shortid.generate)(),
          categories: categories,
          seriesData: preSeries.map(function (_ref5) {
            var data = _ref5.data,
                preSeries = _objectWithoutProperties(_ref5, _excluded2);

            return _objectSpread(_objectSpread({}, preSeries), {}, {
              data: categories.map(function (category) {
                return data.get(category) || {
                  name: category,
                  y: 0
                };
              })
            });
          })
        };
      }, [JSON.stringify(defaultCategories), data]),
          queue = _useMemo.queue,
          categories = _useMemo.categories,
          seriesData = _useMemo.seriesData;

      return (
        /*#__PURE__*/
        // @ts-ignore
        _react["default"].createElement(ChartElement, _extends({
          key: queue,
          ref: ref,
          type: "chart"
        }, props, {
          options: (0, _merge2["default"])(_objectSpread(_objectSpread({}, options), {}, {
            series: seriesData
          }), {
            xAxis: {
              categories: categories
            }
          })
        }))
      );
    });

    SeriesData.displayName = 'AppcraftSeriesData'; // @ts-ignore

    SeriesData.Naked = ChartElement; // @ts-ignore

    SeriesData.overrideds = ['type', 'options.series', 'options.xAxis.categories'];
    return SeriesData;
  };
};

withSeriesData.configTypes = CONFIG_PROP_TYPES;
var _default = withSeriesData;
exports["default"] = _default;