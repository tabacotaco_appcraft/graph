import React, { useState } from 'react';
import PropTypes from 'prop-types';

import Button from '@material-ui/core/Button';
import StyleIcon from '@material-ui/icons/Style';
import { makeStyles } from '@material-ui/core/styles';

import { Map as MaplibreGl } from 'maplibre-gl';

import { SettingToolbar, useMap } from './_SettingWrapper';
import { useLocales } from '../_utils/makeLocales';


export const getDefaultStyle = (defaultStyle, StylePickerProps) => (
  StylePickerProps?.options.find(({ id }, i) => ((!StylePickerProps.value && i === 0) || id === StylePickerProps.value))?.style
    || defaultStyle
      || 'https://api.maptiler.com/maps/streets/style.json?key=get_your_own_OpIi9ZULNHzrESv6T2vL'
);


//* Custom Hooks
const useStyles = makeStyles((theme) => ({
  button: {
    border: '0 !important',

    '& + button': {
      marginLeft: theme.spacing(1)
    }
  }
}));


//* Component
export default function StylePicker({ options, value: defaultValue, onChange }) {
  /** @type {MaplibreGl} */
  const map = useMap();
  const { getFixedT: gt } = useLocales();
  const [value, setValue] = useState(defaultValue || options[0]?.id);
  const classes = useStyles();

  return !options?.length
    ? null
    : (
      <SettingToolbar tooltip={gt('btn-style')} icon={(<StyleIcon />)}>
        {options.map(({ id, icon, description, style }) => (
          <Button
            key={id}
            variant="text"
            color={id === value ? 'primary' : 'default'}
            classes={{ root: classes.button }}
            startIcon={icon}
            onClick={() => {
              if (id !== value) {
                map.setStyle(typeof style === 'string' ? style : { version: 8, ...style });
                setValue(id);
                onChange?.(id, style || null);
              }
            }}
          >
            {description}
          </Button>
        ))}
      </SettingToolbar>
    );
};

StylePicker.propTypes = {
  value: PropTypes.string,
  onChange: PropTypes.func,

  options: PropTypes.arrayOf(
    PropTypes.exact({
      id: PropTypes.string.isRequired,
      icon: PropTypes.element,
      description: PropTypes.string.isRequired,

      //* 相關設置可參考: https://docs.mapbox.com/mapbox-gl-js/example/
      style: PropTypes.oneOfType([
        PropTypes.string.isRequired,

        PropTypes.shape({
          layers: PropTypes.arrayOf(
            PropTypes.shape({
              id: PropTypes.string.isRequired,
              source: PropTypes.string.isRequired,
              type: PropTypes.oneOf(['fill', 'line', 'symbol', 'circle', 'heatmap', 'fill-extrusion', 'raster', 'hillshade', 'background']).isRequired
            })
          ),

          sources: PropTypes.objectOf(
            PropTypes.shape({
              description: PropTypes.string.isRequired,
              type: PropTypes.oneOf(['vector', 'raster', 'raster-dem', 'geojson', 'video', 'image']).isRequired,
              url: PropTypes.arrayOf(PropTypes.string),
              urls: PropTypes.arrayOf(PropTypes.string),
              coordinates: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.number))
            }).isRequired
          ).isRequired
        })
      ]).isRequired
    }).isRequired
  ).isRequired
};
