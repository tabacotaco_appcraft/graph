import React, { useRef, useImperativeHandle, useEffect, useState, useMemo } from 'react';
import PropTypes from 'prop-types';

import Avatar from '@material-ui/core/Avatar';
import Chip from '@material-ui/core/Chip';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Popper from '@material-ui/core/Popper';
import SpeedDial from '@material-ui/lab/SpeedDial';
import SpeedDialIcon from '@material-ui/lab/SpeedDialIcon';
import Toolbar from '@material-ui/core/Toolbar';
import Tooltip from '@material-ui/core/Tooltip';
import Typography from '@material-ui/core/Typography';
import { makeStyles, Theme as MuiTheme } from '@material-ui/core/styles';

import AddCircleIcon from '@material-ui/icons/Add';
import MapIcon from '@material-ui/icons/Map';
import NavigationIcon from '@material-ui/icons/Navigation';
import RemoveCircleIcon from '@material-ui/icons/Remove';
import TuneIcon from '@material-ui/icons/Tune';

import _get from 'lodash/get';
import _merge from 'lodash/merge';
import _omit from 'lodash/omit';
import cx from 'clsx';
import { Map as MaplibreGl, MapOptions, StyleSpecification } from 'maplibre-gl';

import DataMarkerController from './_DataMarkerController';
import HeatMapPicker from './_HeatMapPicker';
import PolygonSelection from './_PolygonSelection';
import SettingWrapper from './_SettingWrapper';
import StylePicker, { getDefaultStyle } from './_StylePicker';
import makeLocales, { useLocales } from '../_utils/makeLocales';
import { DEVICE_MODE } from '../_utils/withHorizontalSlide';
import { useGradientColors } from '../_utils/colors';

import LocalesEn from '../_assets/locales/en/geography.json';
import LocalesZh from '../_assets/locales/zh/geography.json';

import 'maplibre-gl/dist/maplibre-gl.css';
import _ from 'lodash';


//* TS Namespace
const PROP_TYPES = {
  DataMarkerControllerProps: PropTypes.shape(DataMarkerController.propTypes),
  HeatMapPickerProps: PropTypes.shape(HeatMapPicker.propTypes),
  PolygonSelectionProps: PropTypes.shape({ ..._omit(PolygonSelection.propTypes, ['heating']) }),
  StylePickerProps: PropTypes.shape(StylePicker.propTypes),

  className: PropTypes.string,

  classes: PropTypes.exact({
    root: PropTypes.string,
    control: PropTypes.string,
    setting: PropTypes.string
  }),

  data: PropTypes.arrayOf(
    PropTypes.shape({
      coordinates: PropTypes.arrayOf(PropTypes.number).isRequired,
      properties: PropTypes.object,
    })
  ),

  images: PropTypes.objectOf(
    PropTypes.string.isRequired
  ),

  options: PropTypes.shape({
    center: PropTypes.arrayOf(PropTypes.number).isRequired as PropTypes.Validator<[number, number]>,
    zoom: PropTypes.number.isRequired,

    style: PropTypes.oneOfType([
      PropTypes.string,

      PropTypes.shape({
        layers: PropTypes.arrayOf(
          PropTypes.shape({
            id: PropTypes.string.isRequired,
            source: PropTypes.string.isRequired,
            type: PropTypes.oneOf(['fill', 'line', 'symbol', 'circle', 'heatmap', 'fill-extrusion', 'raster', 'hillshade', 'background']).isRequired
          })
        ),

        sources: PropTypes.objectOf(
          PropTypes.shape({
            description: PropTypes.string.isRequired,
            type: PropTypes.oneOf(['vector', 'raster', 'raster-dem', 'geojson', 'video', 'image']).isRequired,
            url: PropTypes.arrayOf(PropTypes.string),
            urls: PropTypes.arrayOf(PropTypes.string),
            coordinates: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.number))
          }).isRequired
        ).isRequired
      })
    ])
  })
};

namespace AppcraftGeography {
  export namespace def {
    export type Props = PropTypes.InferProps<typeof PROP_TYPES>;

    export type StyleProps = {
      bearing: number;
      pitch: number;
      gradient: PropTypes.InferProps<typeof HeatMapPicker.propTypes>['gradientColors'];
      open: boolean;
    };
  }
}


//* Custom Hooks
const useMapboxGlCreate = (
  ref: React.ForwardedRef<MaplibreGl>,
  images: Record<string, string>,
  options: MapOptions,
  onSettingClose: () => void
): [React.MutableRefObject<HTMLDivElement>, MaplibreGl, number, number] => {
  const [map, setMap] = useState<MaplibreGl>(null);
  const [bearing, setBearing] = useState(options.bearing || 0);
  const [pitch, setPitch] = useState(options.pitch || 0);
  const containerRef = useRef();
  const { center, zoom, style } = options;

  useImperativeHandle(ref, () => map);
  useMemo(() => map?.setCenter(center), Object.values(center));
  useMemo(() => map?.setZoom(Math.max(map?.getMinZoom(), Math.min(map?.getMaxZoom(), zoom))), [zoom]);

  useEffect(() => {
    if (containerRef.current) {
      map?.remove();

      new MaplibreGl({
        ...options,
        container: containerRef.current,
        crossSourceCollisions: false,
        style: typeof style === 'string' ? style : { version: 8, ...style }
        // projection: 'globe'
      }).on('rotate', (e) => (
        setBearing(e.target.getBearing())
      )).on('pitch', (e) => (
        setPitch(e.target.getPitch())
      )).on('style.load', (e) => (
        Object.entries(images || {}).forEach(([name, url]) => ( // @ts-ignore
          e.target.loadImage(url, (err, img) => {
            if (err) throw err;
            e.target.addImage(name, img)
          })
        ))
      )).on('load', (e) => {
        setMap(e.target);
        e.target.getCanvasContainer().addEventListener('click', onSettingClose);
      });
    }

    return () => {
      try {
        map?.remove();
      } catch (e) { }

      setMap(null);
    };
  }, []);

  return [containerRef, map, bearing, pitch];
};

// @ts-ignore
const useStyles = makeStyles<MuiTheme, AppcraftGeography.def.StyleProps>((theme) => ({
  root: {
    position: 'relative',
    minHeight: '60vh',
    overflow: 'hidden',
    background: theme.palette.background.paper,
    borderRadius: theme.shape.borderRadius,

    '& details.mapboxgl-ctrl-attrib': {
      display: 'none !important'
    }
  },
  control: {
    position: 'absolute',
    bottom: theme.spacing(0),
    left: '50%',
    transform: 'translateX(-50%)',
    zIndex: theme.zIndex.appBar,
    background: 'none !important',

    '&::before': {
      content: '""',
      position: 'absolute',
      bottom: 0,
      left: '50%',
      top: '50%',
      transform: `translate(-50%, -${theme.spacing(1.5)}px)`,
      backdropFilter: `blur(${theme.spacing(2)}px)`,
      borderTopLeftRadius: '50%',
      borderTopRightRadius: '50%',
      width: 200,
      height: 200
    },

    '& button + *': {
      margin: theme.spacing(0, 0, 0, 0.5)
    },
    '& > button[role^=zoom]': {
      background: theme.palette.grey[800],
      borderRadius: '50%',
      color: theme.palette.getContrastText(theme.palette.grey[800]),
      fontSize: '1.5rem'
    },
    '& > *[role=compass] > button': {
      backdropFilter: `blur(${theme.spacing(2)}px)`,
      borderRadius: '50%',
      color: theme.palette.error.dark,
      fontSize: '3rem',

      '& svg': {
        transform: ({ bearing, pitch }) => `rotate3d(1, 0, 0, ${pitch}deg) rotate3d(0, 0, 1, ${0 - bearing}deg)`
      },
      '& + span': {
        transform: `scale(0.7) translateY(${theme.spacing(-1)}px)`,

        '& > *': {
          background: theme.palette.grey[800],
          color: theme.palette.getContrastText(theme.palette.grey[800])
        }
      }
    }
  },
  setting: {
    position: 'absolute',
    top: theme.spacing(2),
    left: theme.spacing(2),
    zIndex: theme.zIndex.appBar,

    '& > button': {
      background: theme.palette.grey[900],
      color: theme.palette.getContrastText(theme.palette.grey[900]),

      '&:hover': {
        background: theme.palette.grey[800]
      },
      '& + div[role=menu]': {
        width: theme.spacing(7),

        '& > * + *': {
          marginTop: theme.spacing(1)
        },
        '& button': {
          textTransform: 'capitalize'
        }
      }
    }
  },
  legend: {
    position: 'absolute !important',
    top: `${theme.spacing(3.5)}px !important`,
    left: `${theme.spacing(10)}px !important`,
    zIndex: theme.zIndex.tooltip,

    '& > *[role=legend]': {
      background: ({ open }) => theme.palette.grey[open ? 800 : 900],
      color: theme.palette.getContrastText(theme.palette.grey[900]),
      borderRadius: `${theme.spacing(1.5)}px / 50% !important`,
      minHeight: theme.spacing(3.5),
      width: theme.spacing(20),
      padding: theme.spacing(0.25, 1.5),

      '& > *': {
        margin: theme.spacing(0, 0.5),

        '&[role=gradient]': {
          flexGrow: 1,
          width: ({ gradient = [] }) => theme.spacing(2 * gradient.length),
          background: ({ gradient = [] }) => `linear-gradient(to right, ${gradient.map((color) => color)})`,
          borderRadius: theme.spacing(0.25),
          height: theme.spacing(2.5),
          margin: theme.spacing(0, 0.5)
        }
      }
    }
  },
  legendtip: {
    padding: 0,

    '& > div[role=container]': {
      width: 'fit-content',

      '& > div[role=colors]': {
        borderLeft: `1px solid ${theme.palette.divider}`,
        borderTopLeftRadius: theme.shape.borderRadius,
        borderBottomLeftRadius: theme.shape.borderRadius,
        background: ({ gradient = [] }) => `linear-gradient(to top, ${gradient.map((color) => color)})`,
        width: theme.spacing(2.5)
      },
      '& > *[role=tip]': {
        display: 'flex',
        flexDirection: 'column-reverse'
      }
    }
  }
}));


//* Component
const Geography = React.forwardRef<MaplibreGl, AppcraftGeography.def.Props>(({
  DataMarkerControllerProps,
  HeatMapPickerProps,
  PolygonSelectionProps,
  StylePickerProps,

  className,
  classes: cls,
  images,
  options
}, ref) => {
  const { getFixedT: gt } = useLocales();
  const [open, setOpen] = useState(false);
  const [heatMapMode, setHeatMapMode] = useState(HeatMapPickerProps?.value || null);

  const [containerRef, map, bearing, pitch] = useMapboxGlCreate(
    ref,
    images,
    { ...(options as MapOptions), style: getDefaultStyle(options.style, StylePickerProps) as StyleSpecification },
    () => setOpen(false)
  );

  const heatMapOptions = useMemo(() => (
    Object.entries({ polygon: Boolean(PolygonSelectionProps), point: Boolean(HeatMapPickerProps) }).reduce(
      (result, [opt, validation]) => {
        if (!validation) {
          delete result[opt];
        }

        return result;
      },
      HeatMapPickerProps.options as Record<string, any>
    )
  ), [Boolean(PolygonSelectionProps), Boolean(HeatMapPickerProps)]);

  const [gradient, range] = useGradientColors(HeatMapPickerProps.gradientColors) as [string[], number];
  const classes = useStyles({ bearing, pitch, gradient, open });

  return (
    <div ref={containerRef} className={cx(classes.root, className, cls?.root)}>
      {( //* Control Bar
        <Toolbar disableGutters variant="dense" className={cx(classes.control, cls?.control)} onClick={() => setOpen(false)}>
          <Tooltip title={gt('btn-zoom-in')}>
            <IconButton role="zoom-in" size="small" onClick={() => map.zoomIn()}>
              <AddCircleIcon fontSize="inherit" />
            </IconButton>
          </Tooltip>

          <FormControlLabel
            role="compass"
            labelPlacement="bottom"
            label={(
              <Chip size="small" label={`${(90 - pitch).toFixed(2)}°`} />
            )}
            control={(
              <IconButton size="small" onClick={() => map.resetNorthPitch()}>
                <NavigationIcon fontSize="inherit" />
              </IconButton>
            )}
          />

          <Tooltip title={gt('btn-zoom-out')}>
            <IconButton role="zoom-out" size="small" onClick={() => map.zoomOut()}>
              <RemoveCircleIcon fontSize="inherit" />
            </IconButton>
          </Tooltip>

          {containerRef.current && (
            <Popper open={Boolean(heatMapMode)} className={classes.legend} container={containerRef.current}>
              <Tooltip
                classes={{ tooltip: classes.legendtip }}
                title={(
                  <Grid container role="container">
                    <Grid item role="colors" />

                    <Grid item role="tip" component={List} dense disablePadding>
                      {gradient.reduce<{ node: any[]; num: number; }>(
                        ({ node, num: min }, color, i) => {
                          const max: number = (Math.ceil((min + range) * 100) / 100);

                          node.push(
                            <ListItem key={color} divider role="item">
                              <ListItemText
                                primaryTypographyProps={{ variant: 'caption', color: 'inherit', align: 'center' }}
                                primary={`${Math.max(0, min).toFixed(2)}% ~ ${(i === (gradient.length - 1) ? 100 : (max - 0.01)).toFixed(2)}%`}
                              />
                            </ListItem>
                          );

                          return { node, num: max };
                        },
                        { node: [], num: 0 }
                      ).node}
                    </Grid>
                  </Grid>
                )}
              >
                <Toolbar role="legend">
                  <Typography variant="subtitle2" color="textSecondary" align="center">
                    {gt('lbl-low')}
                  </Typography>

                  <span role="gradient" />

                  <Typography variant="subtitle2" color="textSecondary" align="center">
                    {gt('lbl-high')}
                  </Typography>
                </Toolbar>
              </Tooltip>
            </Popper>
          )}
        </Toolbar>
      )}

      <SettingWrapper {...{ map, open, gradient }}>
          //* Setting Board
        <SpeedDial
          ariaLabel="Map Settings"
          direction="down"
          className={cx(classes.setting, cls?.setting)}
          open={open}
          icon={(
            <SpeedDialIcon
              icon={(<MapIcon />)}
              openIcon={(<TuneIcon />)}
            />
          )}
          {...(DEVICE_MODE === 'touch'
            ? { FabProps: { onClick: () => setOpen(!open) } }
            : { onMouseEnter: () => setOpen(true), onMouseLeave: () => setOpen(false) })
          }
        >
          {StylePickerProps && (
            <StylePicker {...StylePickerProps} />
          )}

          {PolygonSelectionProps && (
            <PolygonSelection {...PolygonSelectionProps} heating={heatMapMode === 'polygon'} />
          )}

          {DataMarkerControllerProps && (
            <DataMarkerController {...DataMarkerControllerProps} />
          )}

          {/* {HeatMapPickerProps && (
            <HeatMapPicker
              {...HeatMapPickerProps}
               // @ts-ignore
              options={heatMapOptions}
              value={heatMapMode}
              onChange={(newMode) => {
                setHeatMapMode(newMode);
                HeatMapPickerProps.onChange?.(newMode);
              }}
            />
          )} */}
        </SpeedDial>
      </SettingWrapper>
    </div>
  );
});

Geography.displayName = 'Geography';
Geography.propTypes = PROP_TYPES;

export default makeLocales({
  en: LocalesEn,
  zh: LocalesZh
})(Geography);
