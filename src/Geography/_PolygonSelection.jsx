import React, { useState, useMemo } from 'react';
import PropTypes from 'prop-types';

import Button from '@material-ui/core/Button';
import LayersIcon from '@material-ui/icons/Layers';
import { makeStyles, useTheme } from '@material-ui/core/styles';

import _get from 'lodash/get';
import _merge from 'lodash/merge';
import _template from 'lodash/template';
import axios from 'axios';
import { Map as MaplibreGl } from 'maplibre-gl';

import { PROPERTY_VALUE_SPECIFICATION_PROPTYPES } from './_customs';
import { SettingToolbar, useLayerHandles } from './_SettingWrapper';
import { getRandomColor } from '../_utils/colors';
import { useLocales } from '../_utils/makeLocales';


//* Custom Hooks
const useAreaLayers = (heating, options, values) => {
  const theme = useTheme();
  const sourceIds = useMemo(() => Array.from(values), [values]);

  return useLayerHandles(
    { category: 'area', sourceIds },

    /** Create Source & Add Generate Layers
     *
     *  @param {'area'} category
     *  @param {string} sourceId
     *  @param {{ map: MaplibreGl; }} context
     */
    async (category, sourceId, { LABEL_COLOR, loadSource, addLayerGroup }) => {
      const haloColor = theme.palette.getContrastText(LABEL_COLOR);
      const { label, geojson } = options.find(({ id: optionId }) => optionId === sourceId);
      const { type, features } = typeof geojson !== 'string' ? geojson : (await axios.get(geojson).then(({ data }) => data));

      const source = loadSource(category, sourceId, {
        data: {
          type,
          features: features.map(({ properties, ...feature }) => {
            const { 'fill-outline-color': border, 'fill-color': bg, 'fill-opacity': opacity } = properties;

            return {
              ...feature,
              properties: {
                ...properties,
                [`${sourceId}_boder`]: border || theme.palette.grey[800],
                [`${sourceId}_bg`]: getRandomColor(bg),
                [`${sourceId}_opacity`]: opacity || theme.palette.action.activatedOpacity
              }
            };
          })
        }
      });

      addLayerGroup(sourceId, [{
        type: 'fill',
        id: 'polygon',
        filter: ['==', '$type', 'Polygon'],
        layout: { 'fill-sort-key': 1 },
        paint: {
          'fill-outline-color': ['get', `${sourceId}_boder`],
          'fill-color':  ['get', `${sourceId}_bg`],
          'fill-opacity': ['get', `${sourceId}_opacity`]
        }
      }, {
        ..._merge({
          layout: {
            'text-pitch-alignment': 'map'
          },
          paint: {
            'text-color': LABEL_COLOR,
            'text-halo-color': haloColor,
            'text-halo-width': 2
          }
        }, label),
        type: 'symbol',
        id: 'label'
      }]);

      return source;
    },

    () => [{
      layer: 'polygon',
      mouseenter: (_layerId, { target: map }) => map.getCanvas().style.cursor = 'pointer',
      mouseleave: (_layerId, { target: map }) => map.getCanvas().style.cursor = ''
    }]
  );
};

const useStyles = makeStyles((theme) => ({
  button: {
    border: '0 !important',

    '& + button': {
      marginLeft: theme.spacing(1)
    }
  }
}));


//* Component
export default function PolygonSelection({ heating = false, options, value: defaultValue, onChange }) {
  const { getFixedT: gt } = useLocales();
  const [values, setValues] = useState(() => new Set(Array.isArray(defaultValue) ? defaultValue : defaultValue ? [defaultValue] : []));
  const [addAreaLayer, removeAreaLayer] = useAreaLayers(heating, options, values);
  const classes = useStyles();

  return !options?.length
    ? null
    : (
      <SettingToolbar tooltip={gt('btn-area')} icon={(<LayersIcon />)}>
        {options.map(({ id, icon, description }) => (
          <Button
            key={id}
            variant="text"
            color={values.has(id) ? 'primary' : 'default'}
            classes={{ root: classes.button }}
            startIcon={icon}
            onClick={() => {
              const news = new Set(values);

              if (!values.has(id)) {
                addAreaLayer(id);
                news.add(id);
                onChange?.(id, sourceData);
              } else { //* 移除 Layer
                removeAreaLayer(id);
                news.delete(id);
                onChange?.(id, null);
              }

              setValues(news);
            }}
          >
            {description}
          </Button>
        ))}
      </SettingToolbar>
    );
}

PolygonSelection.propTypes = {
  heating: PropTypes.bool,
  onChange: PropTypes.func,

  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.arrayOf(PropTypes.string)
  ]),
  options: PropTypes.arrayOf(
    PropTypes.exact({
      id: PropTypes.string.isRequired,
      icon: PropTypes.element,
      description: PropTypes.string.isRequired,

      //* 相關設置內容可參考: https://docs.mapbox.com/mapbox-gl-js/example/variable-label-placement/
      label: PropTypes.shape({
        layout: PropTypes.shape({
          'icon-image': PROPERTY_VALUE_SPECIFICATION_PROPTYPES,
          'text-field': PROPERTY_VALUE_SPECIFICATION_PROPTYPES.isRequired
        }),
        pain: PropTypes.shape({
          'icon-opacity': PropTypes.number,
          'icon-color': PropTypes.string,
          'text-opacity': PropTypes.number,
          'text-color': PropTypes.string,
        })
      }),
      geojson: PropTypes.oneOfType([
        //* URL, 使用 string 定義取得 GeoJSON 的網址, 僅支援 Get
        PropTypes.string.isRequired,

        //* FeatureCollection, 使用 object
        PropTypes.exact({
          type: PropTypes.oneOf(['FeatureCollection']).isRequired,

          features: PropTypes.arrayOf(
            PropTypes.shape({
              type: PropTypes.oneOf(['Feature']).isRequired,

              properties: PropTypes.shape({
                'fill-color': PropTypes.string,
                'fill-opacity': PropTypes.number,
                'fill-outline-color': PropTypes.string
              }),
              geometry: PropTypes.shape({
                type: PropTypes.string.isRequired,

                coordinates: PropTypes.arrayOf(
                  PropTypes.arrayOf(
                    PropTypes.oneOfType([
                      PropTypes.number.isRequired,
                      PropTypes.arrayOf(PropTypes.number.isRequired).isRequired
                    ]).isRequired
                  ).isRequired
                ).isRequired
              })
            }).isRequired
          ).isRequired
        }).isRequired
      ]).isRequired
    })
  )
};
