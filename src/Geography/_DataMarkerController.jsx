import React, { useEffect, useState, useMemo, useCallback, useReducer } from 'react';
import PropTypes from 'prop-types';

import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Divider from '@material-ui/core/Divider';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';
import Popover from '@material-ui/core/Popover';
import RoomOutlinedIcon from '@material-ui/icons/RoomOutlined';
import { makeStyles, withStyles, useTheme } from '@material-ui/core/styles';

import _set from 'lodash/set';
import { generate as uuid } from 'shortid';
import { Map as MaplibreGl } from 'maplibre-gl';

import { SettingToolbar, useGradient, useLayerHandles } from './_SettingWrapper';
import { useLocales } from '../_utils/makeLocales';


const DEFAULT_GROUP = uuid();
const LAYOUT_MODES = ['standard', 'track', 'heatmap-point', 'heatmap-area'];


//* Custom Hooks
const useGroupOptions = (() => {
  function reducer(state, { type, id, ...options }) {
    switch (type) {
      case 'databound': {
        const { sourceIds } = options;

        return sourceIds.map((sourceId) => {
          const { mode = 'standard', enabled = true } = state.find(({ id: groupId }) => groupId === sourceId) || {};

          return { id: sourceId, mode, enabled };
        });
      }
      case 'visible':
        return state.map((group) => group.id === id ? _set(group, 'enabled', true) : group);

      case 'hidden':
        return state.map((group) => group.id === id ? _set(group, 'enabled', false) : group);

      case 'mode':
        return state.map((group) => group.id === id ? { ...group, ...options } : group)

      default:
    }

    return state;
  }

  return (sourceIds, value, onChange) => {
    const [values, dispatch] = useReducer(
      reducer,
      sourceIds.map((sourceId) => {
        const { mode = 'standard', enabled = true } = (value || []).find(({ id }) => id === sourceId) || {};

        return { id: sourceId, mode, enabled };
      })
    );

    useEffect(() => {
      onChange?.(
        values.reduce(
          (result, { id, mode, enabled }) => (
            enabled
              ? result.concat({ id, mode })
              : result
          ),
          []
        )
      );
    }, [values]);

    return [values, dispatch];
  };
})();

const useMarkerLayers = (cluster, data) => {
  const gradient = useGradient();
  const theme = useTheme();

  const clusters = useMemo(() => {
    const radius = 30;
    const { base = 100, multiple = 5 } = cluster || {};

    return gradient.reduce(
      (result, color, i) => {
        if (i < (gradient.length - 1)) {
          const distance = base * Math.max(1, multiple * i);

          return {
            color: result.color.concat(color, distance),
            radius: result.radius.concat(radius + (10 * i), distance)
          };
        }

        return {
          color: result.color.concat(color),
          radius: result.radius.concat(radius + (10 * i))
        };
      },
      { color: [], radius: [] }
    );
  }, [])

  const [sourceIds, sources] = useMemo(() => {
    const collection = data.reduce(
      (result, { id, coordinates, group, icon, label, metadata }) => {
        const sourceId = group || DEFAULT_GROUP;
        const features = result.get(sourceId) || [];

        features.push(
          ...coordinates
            .map(({ lng, lat, ts }, seq) => {
              const [fm, to = fm] = (Array.isArray(ts) ? ts : [ts]).map(($ts) => new Date($ts || undefined).valueOf());

              return {
                type: 'Feature',
                geometry: {
                  type: 'Point',
                  coordinates: [lng, lat]
                },
                properties: {
                  ...metadata,
                  [`${sourceId}_group`]: sourceId,
                  [`${sourceId}_icon`]: icon,
                  [`${sourceId}_id`]: id,
                  [`${sourceId}_label`]: label,
                  [`${sourceId}_seq`]: seq,
                  [`${sourceId}_ts`]: { fm, to }
                }
              };
            })
            .sort(({ properties: { [`${sourceId}_ts`]: ts1 } }, { properties: { [`${sourceId}_ts`]: ts2 }}) => (
              ts1.fm - ts2.to
            ))
        );

        return result.set(sourceId, features);
      },
      new Map()
    );

    return [
      Array.from(collection.keys()),
      collection
    ];
  }, [data]);

  return [
    sourceIds,

    ...useLayerHandles(
      { category: 'marker', sourceIds, autofit: true },

      /** Create Source & Add Generate Layers
       *
       *  @param {'marker'} category
       *  @param {string} sourceId
       *  @param {{ map: MaplibreGl; }} context
       */
      (category, sourceId, { LABEL_COLOR, loadSource, addLayerGroup }) => {
        const haloColor = theme.palette.getContrastText(LABEL_COLOR);
        const source = loadSource(category, sourceId, { cluster: true, clusterMaxZoom: 14, data: { type: 'FeatureCollection', features: sources.get(sourceId) } });

        addLayerGroup(sourceId, [
          //* Cluster Circle Layer
          {
            type: 'circle',
            id: 'cluster',
            filter: ['has', 'point_count'],
            paint: {
              'circle-color': ['step', ['get', 'point_count'], ...clusters.color],
              'circle-opacity': .8,
              'circle-pitch-alignment': 'map',
              'circle-radius': ['step', ['get', 'point_count'], ...clusters.radius],
              'circle-stroke-color': haloColor,
              'circle-stroke-width': 2
            }
          },

          //* Cluster Count Text Layer
          {
            type: 'symbol',
            id: 'count',
            filter: ['has', 'point_count'],
            paint: {
              'text-color': LABEL_COLOR,
              'text-halo-color': haloColor,
              'text-halo-width': 2,
              'text-opacity': .8
            },
            layout: {
              'symbol-z-order': 'viewport-y',
              'text-allow-overlap': true,
              'text-field': '{point_count_abbreviated}',
              'text-size': 16
            }
          },

          //* Marker Layer
          {
            type: 'symbol',
            id: 'pin',
            filter: ['all', ['==', `${sourceId}_group`, sourceId], ['==', `${sourceId}_seq`, 0]],
            paint: {
              'text-color': LABEL_COLOR,
              'text-halo-color': haloColor,
              'text-halo-width': 2
            },
            layout: {
              'symbol-z-order': 'viewport-y',
              'icon-allow-overlap': true,
              'icon-anchor': 'bottom',
              'icon-image': ['get', `${sourceId}_icon`],
              'icon-size': 0.8,
              'text-allow-overlap': true,
              'text-anchor': 'top',
              'text-field': ['get', `${sourceId}_label`],
              'text-size': 14
            }
          }
        ]);

        return Promise.resolve(source);
      },

      (source, { map, resortLayers }) => [{
        layer: 'cluster',
        dblclick: (layerId, e) => {
          const [{ geometry, properties }] = map.queryRenderedFeatures(e.point, { layers: [layerId] });

          source.getClusterExpansionZoom(properties.cluster_id, (err, zoom) => {
            if (!err) {
              map.easeTo({ zoom, center: geometry.coordinates });
              resortLayers();
            }
          })
        }
      }, {
        layer: ['pin', 'cluster'],
        mouseenter: () => map.getCanvas().style.cursor = 'pointer',
        mouseleave: () => map.getCanvas().style.cursor = ''
      }]
    )
  ];
};

const useStyles = makeStyles((theme) => ({
  group: {
    '& + *[role=group]': {
      marginLeft: theme.spacing(1)
    }
  },
  button: {
    border: '0 !important'
  }
}));


//* Components
const FitContentMenu = withStyles((theme) => ({
  root: {
    marginTop: theme.spacing(1),
    width: 'fit-content !important'
  }
}))(MenuList);

export default function DataMarkerController({ cluster, lables, data = [], value, onChange }) {
  const { getFixedT: gt } = useLocales();
  const [sourceIds, addMarkerLayer, removeMarkerLayer] = useMarkerLayers(cluster, data);
  const [values, dispatch] = useGroupOptions(sourceIds, value, onChange);
  const [expanded, setExpanded] = useState(null);
  const classes = useStyles();

  return !data.length
    ? null
    : (
      <SettingToolbar tooltip={gt('btn-marker')} icon={(<RoomOutlinedIcon />)}>
        {values.map(({ id, enabled, mode }, i) => (
          <React.Fragment key={id}>
            {i > 0 && (
              <Divider flexItem orientation="vertical" />
            )}

            <ButtonGroup key={id} role="group" variant="text" className={classes.group} color={enabled ? 'primary' : 'default'}>
              <Button
                classes={{ root: classes.button }}
                onClick={() => {
                  dispatch({ type: enabled ? 'hidden' : 'visible', id });
                  enabled ? removeMarkerLayer(id) : addMarkerLayer(id);
                }}
              >
                {lables?.[id] || id}
              </Button>

              <Button onClick={({ currentTarget }) => setExpanded({ anchorEl: currentTarget.closest('[role=group]'), id, mode })}>
                <ArrowDropDownIcon />
              </Button>
            </ButtonGroup>
          </React.Fragment>
        ))}

        <Popover
          anchorEl={expanded?.anchorEl}
          anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
          transformOrigin={{ vertical: 'top', horizontal: 'center' }}
          open={Boolean(expanded)}
          onClose={() => setExpanded(null)}
          PaperProps={{ component: FitContentMenu }}
        >
          {LAYOUT_MODES.map((layout) => (
            <MenuItem
              key={layout}
              selected={layout === expanded?.mode}
              onClick={() => {
                dispatch({ type: 'mode', id: expanded.id, mode: layout });
                setExpanded(null);
              }}
            >
              {gt(`lbl-layout-${layout}`)}
            </MenuItem>
          ))}
        </Popover>
      </SettingToolbar>
    );
}

DataMarkerController.propTypes = {
  lables: PropTypes.objectOf(PropTypes.node.isRequired),
  onChange: PropTypes.func,

  cluster: PropTypes.exact({
    base: PropTypes.number,
    multiple: PropTypes.number
  }),
  value: PropTypes.arrayOf(
    PropTypes.exact({
      id: PropTypes.string.isRequired,
      mode: PropTypes.oneOf(LAYOUT_MODES)
    })
  ),
  data: PropTypes.arrayOf(
    PropTypes.exact({
      id: PropTypes.string.isRequired,
      group: PropTypes.string,
      icon: PropTypes.string.isRequired,
      label: PropTypes.string,
      metadata: PropTypes.object,

      coordinates: PropTypes.arrayOf(
        PropTypes.exact({
          lat: PropTypes.number.isRequired,
          lng: PropTypes.number.isRequired,

          ts: PropTypes.oneOfType([
            PropTypes.number,
            PropTypes.instanceOf(Date)
          ])
        }).isRequired
      ).isRequired
    })
  )
};
