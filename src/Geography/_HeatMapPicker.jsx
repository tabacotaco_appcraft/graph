import React, { useState } from 'react';
import PropTypes from 'prop-types';

import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import GradientIcon from '@material-ui/icons/Gradient';
import Switch from '@material-ui/core/Switch';
import Tooltip from '@material-ui/core/Tooltip';
import { makeStyles, useTheme } from '@material-ui/core/styles';

import { Map as MaplibreGl } from 'maplibre-gl';

import { PROPERTY_VALUE_SPECIFICATION_PROPTYPES } from './_customs';
import { SettingToolbar, useMap } from './_SettingWrapper';
import { useLocales } from '../_utils/makeLocales';


const HEAT_MAP_PROP_TYPES = {
  paint: PropTypes.shape({
    'heatmap-weight': PROPERTY_VALUE_SPECIFICATION_PROPTYPES.isRequired,
    'heatmap-radius': PROPERTY_VALUE_SPECIFICATION_PROPTYPES.isRequired
  }).isRequired
};


//* Custom Hooks
const useStyles = makeStyles((theme) => ({
  button: {
    border: '0 !important',

    '& + button': {
      marginLeft: theme.spacing(1)
    }
  },
  checked: {
    transform: `translateX(${theme.spacing(2)}px) !important`
  }
}));


//* Component
/** @param {{ map: MaplibreGl }} props */
export default function HeatMapPicker({ options, value = null, onChange }) {
  const { getFixedT: gt } = useLocales();
  const [selected, setSelected] = useState(value || Object.keys(options)[0]);

  const classes = useStyles();

  return (
    <SettingToolbar tooltip={gt('btn-heatmap')} icon={(<GradientIcon />)}>
      <Tooltip title={gt(`lbl-heatmap-${value || 'off'}`, { mode: value })}>
        <Switch
          classes={{ checked: classes.checked }}
          color="primary"
          size="small"
          checked={Boolean(value)}
          onChange={(e) => onChange(e.target.checked ? selected : null)}
        />
      </Tooltip>

      {value && Object.keys(options).length > 1 && (
        <>
          <Divider role="divider" flexItem orientation="vertical" />

          {Object.entries(options).map(([mode, heatMapProps]) => (
            <Button
              key={mode}
              variant="text"
              color={mode === value ? 'primary' : 'default'}
              classes={{ root: classes.button }}
              onClick={() => {
                setSelected(mode);
                onChange(mode);
              }}
            >
              {gt(`lbl-heatmap-${mode}`)}
            </Button>
          ))}
        </>
      )}
    </SettingToolbar>
  );
}

HeatMapPicker.propTypes = {
  gradientColors: PropTypes.arrayOf(PropTypes.string.isRequired),
  value: PropTypes.oneOf(['polygon', 'point']),

  onChange: PropTypes.func,

  options: PropTypes.exact({
    //* 相關設置可參考: https://docs.mapbox.com/mapbox-gl-js/example/heatmap-layer/
    point: PropTypes.shape(HEAT_MAP_PROP_TYPES),

    area: PropTypes.shape({
      ...HEAT_MAP_PROP_TYPES,
      source: PropTypes.oneOf(['point', 'properties']) //* 當
    })
  }).isRequired
};
