import React, { createContext, useEffect, useState, useMemo, useCallback, useContext } from 'react';
import PropTypes from 'prop-types';
import { useTheme } from '@material-ui/core/styles';

import _get from 'lodash/get';
import _omit from 'lodash/omit';
import _set from 'lodash/set';
import _template from 'lodash/template';
import { Map as MaplibreGl, LngLatBounds, Source } from 'maplibre-gl';

import CollapseToolbar from '../_utils/CollapseToolbar';


const Z_ORDER = [
  'area',
  'marker'
];

//* Custom Hooks
const SettingContext = createContext({
  map: null,
  open: false,
  gradient: [],

  loadSource: () => null,
  getSources: () => null,
  getLayerIds: () => null,
  addLayerGroup: () => null,
  removeLayerGroup: () => null,
  resortLayers: () => null
});

/** @param {MaplibreGl} map */
const useContextValue = (map, open, gradient) => (
  useMemo(() => {
    function getSources(category) {
      const { sources } = map.getStyle();
      const categories = new Set(!category ? [] : Array.isArray(category) ? category : [category]);

      return Object.keys(sources).reduce(
        (result, sourceId) => {
          const source = map.getSource(sourceId);
          const code = _get(source, ['_data', 'properties', 'category']);

          return (categories.size === 0 && code) || categories.has(code)
            ? result.concat(source)
            : result;
        },
        []
      );
    }

    return {
      map,
      open,
      gradient,
      getSources,

      loadSource: (category, sourceId, { ...specification }) => {
        const source = map.getSource(sourceId)
          ? map.getSource(sourceId)
          : map.addSource(sourceId, { type: 'geojson', ...specification }).getSource(sourceId);

        source.setData(_set(source._data, 'properties', { category, layerIds: [] }));

        return source;
      },
      getLayerIds: (sourceId, prefix = []) => {
        const alloweds = (Array.isArray(prefix) ? prefix : [prefix]).map((str) => new RegExp(`^${str}__`));
        const source = map.getSource(sourceId);
        const layerIds = _get(source, ['_data', 'properties', 'layerIds']);

        return (layerIds || []).reduce(
          (result, id) => (
            alloweds.length === 0 || alloweds.some((regexp) => regexp.test(id))
              ? result.concat(id)
              : result
          ),
          []
        );
      },
      addLayerGroup: (sourceId, layers) => {
        const source = map.getSource(sourceId);
        const { features } = source._data;

        source.setData(
          _set(source._data, 'properties.layerIds', layers.map(({ id, ...layer }) => {
            const layerId = `${id}__${sourceId}`;

            map.getLayer(layerId) && map.removeLayer(layerId);
            map.addLayer({ ...layer, id: layerId, source: sourceId });

            return layerId;
          }))
        );
      },
      removeLayerGroup: (sourceId) => {
        const source = map.getSource(sourceId);
        const { _data: { properties: { layerIds } } } = source;

        layerIds.forEach((layerId) => map.removeLayer(layerId));
        source.setData(_set(source._data, 'properties.layerIds', []));
      },
      resortLayers: () => (
        getSources()
          .reduce(
            (result, { _data: { properties: { category, layerIds } } }) => (
              result.concat(
                (layerIds || []).map((id) => ({ id, category }))
              )
            ),
            []
          )
          .sort(({ category: c1, id: id1 }, { category: c2, id: id2 }) => {
            const diff = Z_ORDER.indexOf(c1) - Z_ORDER.indexOf(c2);

            if (diff < 0) {
              map.moveLayer(id1, id2);
            } else if (diff > 0) {
              map.moveLayer(id2, id1);
            }

            return diff;
          })
      )
    };
  }, [map, open, gradient])
);

/** @returns {MaplibreGl} */
export const useMap = () => {
  const { map } = useContext(SettingContext);

  return map;
};

export const useGradient = () => {
  const { gradient } = useContext(SettingContext);

  return gradient;
}

export const useLayerHandles = (() => {
  const getLabelColor = (map) => Object.values(map.style._layers).find(({ id, type }) => /^place_(city|label)/.test(id) && type === 'symbol').getPaintProperty('text-color');

  /**
   *  @param {{ category: 'area' | 'marker'; sourceIds: string[]; autofit?: boolean; }} params
   *  @param {(category: string, sourceId: string, context: any) => Promise<Source>} initialFn
   *  @param {(source: Source, context: any) => ({ layer: string | []; })[]} eventsFn
   */
  return ({ category, sourceIds, autofit = false }, initialFn, eventsFn) => {
    /** @type {{ map: MaplibreGl }} */
    const { map, open: _open, ...context } = useContext(SettingContext);
    const { getSources, getLayerIds, removeLayerGroup, resortLayers } = context;
    const addLayers = useCallback(initialFn, [...Object.values(context), sourceIds]);

    //* 當 Style 變動時, 需重新載入 GeoJSON
    useEffect(() => {
      const render = () => (
        Promise.all(
          sourceIds.map((sourceId) => addLayers(category, sourceId, { ...context, map, LABEL_COLOR: getLabelColor(map) }))
        ).then((sources) => (
          sources.forEach((source) => (
            eventsFn?.(source, { ...context, category, map }).forEach(({ layer, ...handles }) => (
              (Array.isArray(layer) ? layer : [layer]).forEach((prefix) => (
                getLayerIds(source.id, prefix).forEach((layerId) => (
                  Object.entries(handles).forEach(([eventName, handleFn]) => {
                    const fn = (...e) => handleFn(layerId, ...e);

                    map
                      .once(`${category}.loaded`, () => map.on(eventName, layerId, fn))
                      .once(`${category}.destroy`, () => map.off(eventName, layerId, fn));
                  })
                ))
              ))
            ))
          ))
        )).then(() => (
          resortLayers()
        )).then(() => (
          //* Auto Fit
          autofit && map.fitBounds(
            getSources(category).reduce(
              (bounds, { _data: { features } }) => {
                features.forEach(({ geometry: { type, coordinates } }) => {
                  switch (type) {
                    case 'Point': {
                      bounds.extend(coordinates);
                      break;
                    }
                    default: {
                      coordinates.flat().forEach((coordinate) => (
                        bounds.extend(coordinate)
                      ))
                    }
                  }
                });

                return bounds;
              },
              new LngLatBounds()
            )
          )
        )).then(() => (
          map.fire(`${category}.loaded`)
        ))
      );

      //* 初始載入 GeoJSON
      render();
      map.on('style.load', render);

      return () => {
        map.off('style.load', render);
        map.fire(`${category}.destroy`);
      };
    }, [sourceIds]);

    return [
      useCallback((sourceId) => (
        addLayers(category, sourceId, { ...context, map, LABEL_COLOR: getLabelColor(map) })
      ), [context]),

      removeLayerGroup
    ];
  };
})();


//* Components
export function SettingToolbar(props) {
  const { map, open } = useContext(SettingContext);
  const theme = useTheme();

  return (
    <CollapseToolbar
      {...props}
      container={map.getContainer().querySelector('[aria-label="Map Settings"]')}
      open={open}
      maxWidth={map.getContainer().getBoundingClientRect().width - theme.spacing(4)}
    />
  );
}

export default function SettingWrapper({ children, map, open, gradient }) {
  const value = useContextValue(map, open, gradient);

  return (
    <SettingContext.Provider value={value}>
      {map && (
        children
      )}
    </SettingContext.Provider>
  );
}

SettingWrapper.propTypes = {
  children: PropTypes.node.isRequired,
  gradient: PropTypes.arrayOf(PropTypes.string.isRequired),
  map: PropTypes.instanceOf(MaplibreGl),
  open: PropTypes.bool.isRequired
};
