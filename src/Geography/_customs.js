import PropTypes from 'prop-types';

export const PROPERTY_VALUE_SPECIFICATION_PROPTYPES = PropTypes.arrayOf(PropTypes.any.isRequired);
