/* eslint-disable react/prop-types */
import React, { useRef, useImperativeHandle, useEffect, useState, useReducer } from 'react';

import cx from 'clsx';

import TabScrollButton from '@material-ui/core/TabScrollButton';
import { makeStyles } from '@material-ui/core/styles';


export const DEVICE_MODE = matchMedia('(pointer:fine)').matches ? 'pointer' : 'touch';

function getScrollLeft(el, delta) {
  const { scrollWidth, clientWidth, scrollLeft } = el || {};

  return Math.max(0, Math.min(scrollLeft + delta, scrollWidth - clientWidth));
}

//* Custom Hooks
const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
    overflow: 'hidden',
    whiteSpace: 'nowrap',

    '& > *[role=scroll-button]': {
      position: 'sticky',
      backdropFilter: `blur(${theme.spacing(4)}px)`,
      borderRadius: '50%',
      height: theme.spacing(5),
      zIndex: theme.zIndex.appBar + 1,
      display: ({ visible }) => (!visible ? 'none' : null),

      '&:first-child': {
        margin: theme.spacing(0, 1, 0, 0),
        left: 0
      },
      '&:last-child': {
        margin: theme.spacing(0, 0, 0, 1),
        right: 0
      },
      '&.disabled': {
        background: theme.palette.action.disabledBackground,
        opacity: theme.palette.action.disabledOpacity
      }
    }
  }
}));

const useHorizontalSlide = (ref) => {
  const slidedRef = useRef();
  const [scrollLeft, setScrollLeft] = useState(0);
  const [visible, dispatch] = useReducer((_visible, { scrollWidth: sw, clientWidth: cw }) => (sw > cw), false);

  const { scrollWidth, clientWidth } = slidedRef.current || {};
  const classes = useStyles({ visible });

  useImperativeHandle(ref, () => slidedRef.current);

  //* Window Resize 時，判斷是否呈現 Scroll Button
  useEffect(() => {
    const resize = () => dispatch(slidedRef.current);

    global.window?.addEventListener('resize', resize);
    resize();

    return () => global.window?.removeEventListener('resize', resize);
  }, []);

  //* State - scrollLeft 改變時，連帶變更 Element scrollLeft
  useEffect(() => {
    if (slidedRef.current) {
      slidedRef.current.scrollLeft = scrollLeft;
    }
  }, [scrollLeft]);

  return [
    slidedRef,
    classes.root,
    { left: scrollLeft > 0, right: scrollLeft < (scrollWidth - clientWidth) },

    {
      left: () => setScrollLeft(getScrollLeft(slidedRef.current, -100)),
      right: () => setScrollLeft(getScrollLeft(slidedRef.current, 100)),
      wheel: (e) => setScrollLeft(getScrollLeft(slidedRef.current, e.deltaY)),

      touch: (e) => {
        let { touches: [{ clientX: start }] } = e;

        function handleSlide(event) {
          const { changedTouches: [{ clientX: end }] } = event;
          const delta = start - end;

          start = end;
          setScrollLeft(getScrollLeft(slidedRef.current, delta));
        }

        document.body.addEventListener('touchmove', handleSlide);
        document.body.addEventListener('touchend', function end() {
          document.body.removeEventListener('touchmove', handleSlide);
          document.body.removeEventListener('touchend', end);
        });
      }
    }
  ];
};


//* Wrapper
export default function withHorizontalSlide(WrappedEl) {
  const Wrapper = React.forwardRef(({ children, className, ...props }, ref) => {
    const [slidedRef, cls, enabled, handles] = useHorizontalSlide(ref);

    return (
      <WrappedEl {...props} ref={slidedRef} className={cx(className, cls)} onWheel={handles.wheel} onTouchStart={handles.touch}>
        {DEVICE_MODE === 'pointer' && (
          <TabScrollButton
            role="scroll-button"
            direction="left"
            orientation="horizontal"
            classes={{ disabled: 'disabled' }}
            disabled={!enabled.left}
            onClick={handles.left}
          />
        )}

        {children}

        {DEVICE_MODE === 'pointer' && (
          <TabScrollButton
            role="scroll-button"
            direction="right"
            orientation="horizontal"
            classes={{ disabled: 'disabled' }}
            disabled={!enabled.right}
            onClick={handles.right}
          />
        )}
      </WrappedEl>
    );
  });

  Wrapper.displayName = 'HorizontalSlideWrapper';
  Wrapper.Naked = WrappedEl;

  return Wrapper;
}
