import { useMemo } from 'react';

import _get from 'lodash/get';
import _pick from 'lodash/pick';
import * as colors from '@material-ui/core/colors';


const ALLOWED_COLOR = ['amber', 'blue', 'blueGrey', 'brown', 'cyan', 'deepOrange', 'deepPurple', 'green', 'indigo', 'lightBlue', 'lightGreen', 'lime', 'orange', 'pink', 'purple', 'red', 'teal', 'yellow'];
const ALLOWED_CODE = [200, 300, 400, 500, 600, 700, 800, 900, 'A100', 'A200', 'A400', 'A700'];

export const getRandomColor = (defaultColor) => (
  defaultColor || _get(colors, [
    ALLOWED_COLOR[Math.floor(Math.random() * ALLOWED_COLOR.length)],
    ALLOWED_CODE[Math.floor(Math.random() * ALLOWED_CODE.length)]
  ])
)

export function useGradientColors(defaultGradientColors) {
  return useMemo(() => {
    const result = defaultGradientColors || [colors.blue, colors.green, colors.yellow, colors.orange, colors.red].map((color, i) => color[(i + 1) * 100]);

    return [
      result,
      100 / result.length
    ];
  }, []);
}
