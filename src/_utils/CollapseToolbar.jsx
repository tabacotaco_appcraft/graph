import React, { useEffect, useState, useMemo } from 'react';
import PropTypes from 'prop-types';

import Collapse from '@material-ui/core/Collapse';
import Fab from '@material-ui/core/Fab';
import Toolbar from '@material-ui/core/Toolbar';
import Tooltip from '@material-ui/core/Tooltip';
import { makeStyles } from '@material-ui/core/styles';

import cx from 'clsx';
import { generate as uuid } from 'shortid';

import withHorizontalSlide, { DEVICE_MODE } from './withHorizontalSlide';


//* Custom Hooks
const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'row',
    margin: theme.spacing(0, 0.5)
  },
  toolbar: {
    padding: theme.spacing(0, 0.5),
    borderRadius: `${theme.spacing(3)}px / 50% !important`,
    minHeight: theme.spacing(6),
    maxWidth: ({ maxWidth }) => maxWidth || '100%',
    background: theme.palette.grey[800],

    '&:hover': {
      background: theme.palette.grey[700]
    },
    '& > button[role=icon]': {
      maxWidth: theme.spacing(5),
      minWidth: theme.spacing(5)
    },
    '& > *[role=content]': {
      maxWidth: '100%',
      padding: ({ collapsed }) => theme.spacing(0, collapsed ? 0 : 2),
      width: ({ collapsed }) => collapsed ? 0 : 'auto',
      transition: theme.transitions.create(['width', 'padding']),

      '& > *[role=divider]': {
        margin: theme.spacing(0, 2)
      }
    }
  }
}));


//* Component
const HorizontalSlideToolbar = withHorizontalSlide(Toolbar);

/** @param {{ container: HTMLElement }} props */
export default function CollapseToolbar({ className, container, open = false, icon, maxWidth, tooltip, children }) {
  const [collapsed, setCollapsed] = useState(true);
  const id = useMemo(() => uuid(), []);
  const classes = useStyles({ collapsed, maxWidth });

  useEffect(() => {
    setCollapsed(!open ? true : collapsed);
  }, [open]);

  useEffect(() => {
    if (!collapsed) {
      container?.dispatchEvent(new CustomEvent('toggle', { detail: { id } }));
    }
  }, [collapsed]);

  useEffect(() => {
    if (container) {
      const toggleFn = ({ detail: { id: targetId } }) => (targetId !== id && setCollapsed(true));

      container.addEventListener('toggle', toggleFn);

      return () => container.removeEventListener('toggle', toggleFn);
    }
  }, [container]);

  return (
    <Collapse in={open} classes={{ wrapperInner: classes.root }}>
      <Toolbar
        disableGutters
        variant="dense"
        className={cx(classes.toolbar, className)}
        {...(DEVICE_MODE === 'pointer' && { onMouseLeave: () => setCollapsed(true) })}
      >
        <Tooltip title={tooltip}>
          <Fab
            role="icon"
            size="small"
            color={collapsed ? 'default' : 'primary'}
            {...(DEVICE_MODE === 'touch'
              ? { onClick: () => setCollapsed(!collapsed) }
              : { onMouseEnter: () => setCollapsed(false) })}
          >
            {icon}
          </Fab>
        </Tooltip>

        <HorizontalSlideToolbar key={collapsed} role="content" variant="dense">
          {children}
        </HorizontalSlideToolbar>
      </Toolbar>
    </Collapse>
  );
}

CollapseToolbar.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  container: global.window ? PropTypes.instanceOf(HTMLElement) : PropTypes.any,
  icon: PropTypes.element.isRequired,
  maxWidth: PropTypes.number,
  open: PropTypes.bool,
  tooltip: PropTypes.string.isRequired
};
