import React, { createContext, useMemo, useCallback, useContext } from 'react';

import _get from 'lodash/get';
import _template from 'lodash/template';


namespace AppcraftLocale {
  export type Locales = {
    [lang: string]: {
      [code: string]: string;
    };
  };

  export type Context = {
    lang: string;
    locales: Locales;
    defaultLocales: Locales;
    getFixedT: (code: string, params?: Record<string, any>) => string;
  };

  export type Props = {
    lang?: string;
    locales?: AppcraftLocale.Locales;
  }
}

const LocaleContext = createContext<AppcraftLocale.Context>({
  lang: 'en',
  locales: {},
  defaultLocales: {},
  getFixedT: (code) => code
});

export const useLocales = () => useContext(LocaleContext);

export default function makeLocales(defaultLocales: AppcraftLocale.Locales) {
  const useFixedT = (locales: AppcraftLocale.Locales) => (
    useCallback((code, params) => (
      _template(_get(locales, code) || code, { interpolate: /{{([\s\S]+?)}}/g })(params || {})
    ), [locales])
  );

  const useLocales = (lang: string, overridedLocales: AppcraftLocale.Locales) => (
    useMemo(() => {
      const keys = new Set(Object.keys(overridedLocales || {}));
      const keeps = Object.keys(defaultLocales).filter(($lang) => !keys.has($lang));

      const all = Object.entries(overridedLocales || {}).reduce(
        (result, [$lang, options]) => {
          const { alias, fixedT = _get(defaultLocales, $lang) } = typeof options === 'string'
            ? { alias: options }
            : options;

          return {
            ...result,
            [alias]: fixedT
          };
        },
        keeps.reduce(
          (result, $lang) => ({
            ...result,
            [$lang]: _get(defaultLocales, $lang)
          }),
          {}
        )
      );

      return [_get(all, lang), all];
    }, [lang, overridedLocales])
  );

  return <AppcraftProps extends object>(LocaleComponent: React.FC<AppcraftProps> | React.ForwardRefExoticComponent<AppcraftProps>) => (
    React.forwardRef<any, AppcraftProps & AppcraftLocale.Props>(({ lang, locales: overridedLocales, ...props }, ref) => {
      const [locales, allLocales] = useLocales(lang, overridedLocales);
      const getFixedT = useFixedT(locales) as AppcraftLocale.Context['getFixedT'];

      return (
        <LocaleContext.Provider value={{ lang, locales: allLocales, defaultLocales: overridedLocales, getFixedT }}>
          <LocaleComponent ref={ref} {...(props as AppcraftProps)} />
        </LocaleContext.Provider>
      );
    })
  );
}
