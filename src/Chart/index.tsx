import React, { useRef, useImperativeHandle, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { makeStyles, useTheme } from '@material-ui/core/styles';

import _cloneDeep from 'lodash/cloneDeep';
import _create from 'lodash/create';
import _get from 'lodash/get';
import _merge from 'lodash/merge';
import _omit from 'lodash/omit';
import _pick from 'lodash/pick';
import _set from 'lodash/set';
import cm from 'camelcase';
import cx from 'clsx';

import AvocadoTheme from 'highcharts/themes/avocado';
import DarkBlueTheme from 'highcharts/themes/dark-blue';
import DarkGreenTheme from 'highcharts/themes/dark-green';
import DarkUnicaTheme from 'highcharts/themes/dark-unica';
import GrayTheme from 'highcharts/themes/gray';
import GridTheme from 'highcharts/themes/grid';
import Highcharts from 'highcharts';
import SunsetTheme from 'highcharts/themes/sunset';
import map from 'highcharts/modules/map';
import gantt from 'highcharts/modules/gantt';
import stock from 'highcharts/modules/stock';

import withSeriesData from './withSeriesData';


//* TS Namespace
export namespace AppcraftChart {
  export namespace def {
    export const proptypes = {
      className: PropTypes.string,
      options: PropTypes.shape({}).isRequired,

      theme: PropTypes.oneOf([
        'avocado',
        'dark-blue',
        'dark-green',
        'dark-unica',
        'gray',
        'grid',
        'sunset'
      ]),
      type: PropTypes.oneOf([
        'chart',
        'ganttChart',
        'mapChart',
        'stockChart'
      ])
    };

    export type Props = PropTypes.InferProps<typeof proptypes>;

    export interface PropsWithData extends Omit<Props, 'type'> {
      data?: Record<string, any>[];
    }
  }
}


//* Custom Hooks
gantt(Highcharts);
map(Highcharts);
stock(Highcharts);

const useHighchartsCreate = (() => {
  const HIGHCHART_THEME = {
    AvocadoTheme,
    DarkBlueTheme,
    DarkGreenTheme,
    DarkUnicaTheme,
    GrayTheme,
    GridTheme,
    SunsetTheme
  };

  return (
    ref: React.ForwardedRef<Highcharts.Chart>,
    type: AppcraftChart.def.Props['type'],
    options: Highcharts.Options,
    themeCode: AppcraftChart.def.Props['theme']
  ): [React.MutableRefObject<HTMLDivElement>, Highcharts.Chart] => {
    const [highchart, setHighchart] = useState<Highcharts.Chart>(null);
    const containerRef = useRef();
    const theme = useTheme();

    useImperativeHandle(ref, () => highchart, [highchart]);

    useEffect(() => {
      const createFn = _get(Highcharts, type);

      if (!containerRef.current) {
        setHighchart(null);
      } else {
        const withHighchartTheme = _get(HIGHCHART_THEME, `${cm(themeCode || (theme.palette.type === 'dark' ? 'dark-unica' : 'avocado'), { pascalCase: true })}Theme`);

        withHighchartTheme({
          _modules: {
            // @ts-ignore
            ..._get(_pick(Highcharts, [['_modules', 'Core/Globals.js'], ['_modules', 'Core/Utilities.js']]), '_modules'),

            'Core/DefaultOptions.js': {
              defaultOptions: {},
              setOptions: (themeOptions: Record<string, any>) => (
                setHighchart(createFn(containerRef.current, _merge(themeOptions, { chart: { backgroundColor: theme.palette.background.paper } }, options)))
              )
            }
          }
        });
      }

      return () => {
        try {
          highchart?.destroy();
        } catch (e) { }
      };
    }, [containerRef.current, type, themeCode]);

    return [containerRef, highchart];
  };
})();

const useStyles = makeStyles((theme) => {
  const makeFontImportant = ({ fontSize, fontFamily, fontWeight, ...styles }: Record<string, any>) => ({
    ...styles,
    ...(fontSize && { fontSize: `${fontSize} !important` }),
    ...(fontFamily && { fontFamily: `${fontFamily} !important` }),
    ...(fontWeight && { fontWeight: `${fontWeight} !important` }),
    textTransform: 'capitalize !important',
    fill: `${theme.palette.getContrastText(theme.palette.background.paper)} !important`
  });

  return {
    root: {
      background: theme.palette.background.paper,
      borderRadius: theme.shape.borderRadius,
      width: '100%',

      '& text.highcharts-title': makeFontImportant(theme.typography.h6),
      '& text.highcharts-subtitle': makeFontImportant(theme.typography.subtitle2),
      '& text.highcharts-axis-title': makeFontImportant(theme.typography.subtitle2),
      '& text.highcharts-plot-line-label': makeFontImportant(theme.typography.caption),
      '& g.highcharts-legend-item > text': makeFontImportant(theme.typography.subtitle2),
      '& g.highcharts-axis-labels > text': makeFontImportant(theme.typography.body2),

      '& path.highcharts-tick, & path.highcharts-axis-line, & path.highcharts-grid-line': {
        stroke: `${theme.palette.divider} !important`
      },
      '& path.highcharts-label-icon': {
        fill: `${theme.palette.primary.main} !important`
      },
      '& rect.highcharts-background': {
        strokeWidth: '0 !important'
      },
      '& rect.highcharts-plot-background, & rect.highcharts-legend-box': {
        fill: `${theme.palette.background.paper} !important`
      },
      '& text.highcharts-credits': {
        display: 'none !important'
      }
    }
  };
});


//* Component
const Chart = React.forwardRef<Highcharts.Chart, AppcraftChart.def.Props>(({ className, options, theme, type = 'chart' }, ref) => {
  const [containerRef] = useHighchartsCreate(ref, type, options, theme);
  const classes = useStyles();

  return (
    <div ref={containerRef} className={cx(classes.root, className)} />
  );
});

Chart.propTypes = AppcraftChart.def.proptypes;

export default Chart;
export { withSeriesData };
