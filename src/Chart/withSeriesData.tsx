import React, { useMemo } from 'react';
import Highcharts from 'highcharts';
import PropTypes from 'prop-types';

import _get from 'lodash/get';
import _merge from 'lodash/merge';
import _pick from 'lodash/pick';
import _set from 'lodash/set';
import _template from 'lodash/template';
import _toPath from 'lodash/toPath';
import { generate as uuid } from 'shortid';

import Chart, { AppcraftChart } from './index';


//* Definitions
const CONFIG_PROP_TYPES = {
  series: PropTypes.string,
  stack: PropTypes.string,
  x: PropTypes.string,

  aggregate: PropTypes.oneOf<'avg' | 'count' | 'max' | 'min' | 'sum'>([
    'avg', 'count', 'max', 'min', 'sum'
  ]),
  y: PropTypes.arrayOf(
    PropTypes.string
  )
};


//* HOC
const withSeriesData = ({ aggregate = 'sum', series, stack, x, y }: PropTypes.InferProps<typeof CONFIG_PROP_TYPES>) => {
  type Field = { field: string; value: any };
  type CategoryData = { name: string; y: number; properties: Record<string, any>; series: { x: Field; y: Field; name: string; stack?: string; }; };
  type SeriesData = Omit<CategoryData, 'properties' | 'series'>;
  type Series = { name?: string; stack?: string; data: Map<string, SeriesData>; };

  function compiled(template: string, params: Record<string, any>): string {
    const interpolate = /{{([\s\S]+?)}}/g;

    try {
      return _template(interpolate.test(template) ? template : `{{${template}}}`, { interpolate })(params) || null;
    } catch (e) {
      return null;
    }
  }

  function generator(defaultCategories: string[], data: Record<string, any>[]): [Series[], string[]] {
    const categories = new Set(defaultCategories.map((c) => c.toString()));
    const isCategoriesUndifined = categories.size === 0;

    return [
      Array.from(
        data
          //* Split Data
          .reduce<CategoryData[]>(
            (__, rowData) => (
              y.reduce(
                (result, field) => {
                  const categoryName = compiled(x, rowData);

                  if (categoryName && (isCategoriesUndifined || categories.has(categoryName))) {
                    const value = _get(rowData, field) || 0;
                    const refX = { field: x, value: categoryName };
                    const refY = { field, value };
                    const stackName = compiled(stack, { ...rowData, series: { x: refX, y: refY } });

                    if (isCategoriesUndifined) {
                      categories.add(categoryName);
                    }

                    result.push({
                      name: categoryName,
                      properties: rowData,
                      y: value,
                      series: {
                        name: compiled(series, { ...rowData, series: { stack: stackName, x: refX, y: refY } }) || '',
                        stack: stackName,
                        x: refX,
                        y: refY
                      },
                    });
                  }

                  return result;
                },
                __
              )
            ),
            []
          )
          //* Categorize
          .reduce<Map<string, Series>>(
            (result, { series: { name: seriesName, stack: stackName }, properties, name: categoryName, y }) => {
              const key = JSON.stringify({ name: seriesName, stack: stackName });
              const { data: seriesData = new Map<string, SeriesData>() } = result.get(key) || {};
              const categoryData: SeriesData = seriesData.get(categoryName) || { name: categoryName, y: 0 };

              switch (aggregate) {
                case 'avg': {
                  _set(categoryData, 'y', (categoryData.y + y) / 2);
                  break;
                }
                case 'count': {
                  _set(categoryData, 'y', categoryData.y + 1);
                  break;
                }
                case 'max': {
                  _set(categoryData, 'y', Math.max(categoryData.y, y));
                  break;
                }
                case 'min': {
                  _set(categoryData, 'y', Math.min(categoryData.y, y));
                  break;
                }
                default: {
                  _set(categoryData, 'y', categoryData.y + y);
                }
              }

              return result.set(key, {
                data: seriesData.set(categoryName, categoryData),
                ...(seriesName && { name: seriesName }),
                ...(stackName && { stack: stackName })
              });
            },
            new Map()
          ).values()
      ),

      isCategoriesUndifined
        ? Array.from(categories).sort()
        : Array.from(categories)
    ];
  }

  return (ChartElement: typeof Chart) => {
    const SeriesData: React.FC<AppcraftChart.def.PropsWithData> = React.forwardRef(({ data, options, ...props }, ref: React.ForwardedRef<Highcharts.Chart>) => {
      const defaultCategories = _get(options, ['xAxis', 'categories']);

      const { queue, categories, seriesData } = useMemo(() => {
        const [preSeries, categories] = generator(defaultCategories || [], data || []);

        return {
          queue: uuid(),
          categories,
          seriesData: preSeries.map(({ data, ...preSeries }) => ({
            ...preSeries,
            data: categories.map((category) => data.get(category) || { name: category, y: 0 })
          }))
        };
      }, [JSON.stringify(defaultCategories), data]);

      return (
        // @ts-ignore
        <ChartElement key={queue} ref={ref} type="chart" {...props} options={_merge({ ...options, series: seriesData }, { xAxis: { categories } })} />
      );
    });

    SeriesData.displayName = 'AppcraftSeriesData';
    // @ts-ignore
    SeriesData.Naked = ChartElement;
    // @ts-ignore
    SeriesData.overrideds = ['type', 'options.series', 'options.xAxis.categories'];

    return SeriesData;
  };
};

withSeriesData.configTypes = CONFIG_PROP_TYPES;

export default withSeriesData;
