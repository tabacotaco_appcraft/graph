# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.5](https://gitlab.com/tabacotaco_appcraft/graph/compare/v0.0.4...v0.0.5) (2022-08-16)

### [0.0.4](https://gitlab.com/tabacotaco_appcraft/graph/compare/v0.0.3...v0.0.4) (2022-08-16)

### [0.0.3](https://gitlab.com/tabacotaco_appcraft/graph/compare/v0.0.2...v0.0.3) (2022-08-16)

### 0.0.2 (2022-08-16)


### Features

* build package ([dc885d7](https://gitlab.com/tabacotaco_appcraft/graph/commit/dc885d79530a2ce5220db9eec149a42fc65ad88f))
